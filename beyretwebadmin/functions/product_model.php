<?php
require '../config.php';
if (isset($_GET['deleteid'])) {
    if ($_GET['deleteid'] == "all") {
        deleteAllProduct();
    } else {
        deleteProduct($_GET['deleteid']);
    }
} else if (isset($_GET['updateid'])) {
    $name = $_POST['name'];
    $info = $_POST['info'];
    $price = $_POST['price'];
    updateProduct($_GET['updateid'], $name, $info, $price);
} else if (isset($_GET['cancelid'])) {
    if ($_GET['cancelid'] == "all") {
        cancelAllSell();
    } else {
        cancelSell($_GET['cancelid']);
    }

} else if (isset($_GET['sendId'])) {
    markAsSend($_GET['sendId']);
    //deleteProduct($_GET['soldId']);
    done("paidproduct.php");
}

function connect()
{
    $servername = $GLOBALS['servername'];
    $username = $GLOBALS['username'];
    $password = $GLOBALS['password'];
    $dbname = $GLOBALS['dbname'];
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } else {
        return $conn;
    }
}

function run($query)
{
    $conn = connect();
    $result = mysqli_query($conn, $query);
    $conn->close();
    return $result;
}

function done($location)
{
    header("Location:../default/$location");
    exit();
}

function getCompanyName(){
    $query = "Select companyName from general";
    $result = run($query);
    $row = $result->fetch_assoc();
    return $row["companyName"];
}
function addProduct($name, $info, $price)
{
    $img = upload();
    $query = "INSERT INTO products (name, price, info, picture, isavailable) VALUES ('$name', '$price', '$info', '$img',1)";
    run($query);
    done("products.php");
}

function updateProduct($id, $name, $info, $price)
{
    $img = upload();
    $query = "UPDATE products SET name = '$name', info='$info',price='$price',picture='$img' WHERE pid = $id";
    run($query);
    done("products.php");
}

function getProduct($id)
{
    $query = "Select * from products where pid=$id";
    $result = run($query);
    return $result;
}

function deleteProduct($id)
{
    $product = getProduct($id);
    $row = $product->fetch_assoc();
    unlink("../../productimages/" . $row['picture']);
    $query = "delete from products where pid=$id";
    run($query);
    done("products.php");
    
}

function getAllProduct()
{
    $query = "Select * from products";
    $result = run($query);
    return $result;
}

function deleteAllProduct()
{
    $query = "truncate selledproducts";
    run($query);
    $current = getAllProduct();
    while ($row = $current->fetch_assoc()) {
        $id = $row['pid'];
        $query = "delete from products where pid=$id";
        run($query);
    }
    $query = "ALTER TABLE products AUTO_INCREMENT = 1; ";
    run($query);
    clearImageDirectory();
    done("products.php");
}

function checkProductExist()
{
    $result = getProductToSale();
    if (mysqli_num_rows($result)) {
        return true;
    } else {
        return false;
    }
}

function updateProductStatus($id, $isavailable)
{
    $query = "UPDATE products SET isavailable = '$isavailable' WHERE pid = $id";
    run($query);
}

function sellProduct($buyermail, $productid, $lastprice)
{
    if (!checkSellExist($productid)) {
        $product = getProduct($productid);
        $productinfo = $product->fetch_assoc();
        $productname = $productinfo["name"];
        updateProductStatus($productid, 0);

        $buyer = getBuyer($buyermail);
        $buyerinfo = $buyer->fetch_assoc();
        $buyerid = $buyerinfo["uid"];
       // $buyerid = 1;

        //echo $buyerid." ".$buyermail." ".$productid." ".$productname." ".$lastprice." ".$buyermail;
        //echo "--->".$buyerid." ".$buyermail." ".$productid." ".$productname." ".$lastprice." ".$buyermail;
        $query = "INSERT INTO selledproducts (uid, pid, productname, productprice,buyermail,isPaid,isSend) VALUES ($buyerid, $productid, '$productname', $lastprice,'$buyermail',0,0)";
       // $query="INSERT INTO selledproducts (uid, pid, productname, productprice, buyermail, isPaid, isSend) VALUES (1, 1, 'fafdfsfds', 1, 'akakssa', 0, 0)"
        run($query);

    }
   // done("saleproduct.php");
}

function cancelSell($id)
{
    updateProductStatus($id, 1);
    $query = "delete from selledproducts where pid=$id";
    run($query);
    done("saleproduct.php");
}

function cancelAllSell()
{
    $current = getSellingProduct();
    while ($row = $current->fetch_assoc()) {
        updateProductStatus($row['pid'], 1);
    }
    $query = "truncate selledproducts";
    run($query);
    done("saleproduct.php");
}

function checkSellExist($id)
{
    $query = "Select * from selledproducts where pid=$id";
    $result = run($query);
    if (mysqli_num_rows($result)) {
        return true;
    } else {
        return false;
    }
}

function upload()
{
    $source = $_FILES["img"]["tmp_name"];
    $path = $_FILES["img"]["name"];
    $type = $_FILES["img"]["type"];
    $boyut = $_FILES["img"]["size"];
    $target = "../../productimages";
    $save = move_uploaded_file($source, $target . "/" . $path);
    if ($save) {
        return $path;
    } else {
        return "null";
    }
}

function clearImageDirectory()
{
    $files = glob('../../productimages/*'); // get all file names
    foreach ($files as $file) { // iterate files
        if (is_file($file))
            unlink($file); // delete file
    }
}

function getBuyer($email)
{
    $query = "Select * from users where email='$email'";
    $result = run($query);
    return $result;
}

function getAllBuyer()
{
    $query = "Select * from users";
    $result = run($query);
    return $result;
}

function getSellingProduct()
{
    $query = "Select * from selledproducts where isPaid=0";
    $result = run($query);
    return $result;
}

function getPaidProduct()
{
    $query = "Select * from selledproducts where isPaid=1";
    $result = run($query);
    return $result;
}
function markAsSend($id)
{
   
    $query = "UPDATE selledproducts SET isSend = 1 WHERE id = $id";
    run($query);
   
}

function getPaidDetails($id){
    $query = "Select * from paid_details where pid=$id";
    $result = run($query);
    return $result;
}
function getProductToSale()
{
    $query = "Select * from products where isavailable=1";
    $result = run($query);
    return $result;
}

function getAdressInfo($id){
                $query = "Select * from purchaseinfo where id=$id";
                $result = run($query);
                return $result;
 }


?>
