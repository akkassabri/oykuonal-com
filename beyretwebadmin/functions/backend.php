<?php
if(!isset($_SESSION)){
    session_start();
}
require 'config.php';
if(isset($_GET['f'])){
    if(function_exists($_GET['f'])) {
        $_GET['f']($_GET['p']);
    }
}

if(isset($_GET['deleteImageId'])){
    deleteImageFromGallery($_GET['deleteImageId']);
}elseif (isset($_GET['deleteproductid'])) {
    deleteProduct($_GET['deleteproductid']);
}elseif(isset($_GET['deleteserviceid'])){
    deleteService($_GET['deleteserviceid']);
}

if (isset($_POST['productid'])) {
    getProduct($_POST['productid']);
}elseif (isset($_POST['serviceid'])){
    getService($_POST['serviceid']);
}elseif (isset($_POST['userPass'])){
    changeUserPass($_POST['userPass']);
}elseif (isset($_POST['test'])){
    $path = $_FILES["file"]["name"];
    echo $path;
}

if (isset($_GET['disableBlog_id'])) {
    disableBlog($_GET['disableBlog_id']);
}else if (isset($_GET['enableBlog_id'])) {
    enableBlog($_GET['enableBlog_id']);
}else if (isset($_GET['deleteBlog_id'])) {
    deleteBlog($_GET['deleteBlog_id']);
}

if (isset($_GET['disableService_id'])) {
    disableService($_GET['disableService_id']);
}else if (isset($_GET['enableService_id'])) {
    enableService($_GET['enableService_id']);
}else if (isset($_GET['deleteService_id'])) {
    deleteService($_GET['deleteService_id']);
}
/******** GENEL BAŞLANGIÇ *********/
function connect(){
    $servername = $GLOBALS['servername'];
    $username = $GLOBALS['username'];
    $password = $GLOBALS['password'];
    $dbname = $GLOBALS['dbname'];
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } else {
        return $conn;
    }
}

function run($query){
    $conn = connect();
    mysqli_query($conn,"SET NAMES UTF8");
    $result = mysqli_query($conn, $query);
    $conn->close();
    return $result;
}

function done($location){
    header("Location:../$location");
    exit();
}

function fileUpload($target){
    $source = $_FILES["img"]["tmp_name"];
    $path = $_FILES["img"]["name"];
    $path=date("Y-m-d_H:i:s")."_".$path;
    $save = move_uploaded_file($source, $target . "/" . $path);
    if ($save) {
        return $path;
    } else {
        return "null";
    }
}

function slugify($text){
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    $text = preg_replace('~[^-\w]+~', '', $text);
    $text = trim($text, '-');
    $text = preg_replace('~-+~', '-', $text);
    $text = strtolower($text);
    if (empty($text)) {
        return 'n-a';
    }
    return $text;
}

function getActiveModules(){
    $query = "Select * from modules where access=1";
    $result = run($query);
    return $result;
}

function getSubmenus($id){
    $query = "Select * from modules where parent=$id and access=1";
    $result = run($query);
    if (mysqli_num_rows($result)) {
        return $result;
    } else {
        return false;
    }

}

function checkAccess($href){
    $query = "Select * from modules where href='$href' and access=0";
    $result = run($query);
    if (mysqli_num_rows($result)) {
        header("Location:../403.html");
    }
}
/******** GENEL BİTİŞ *********/

/******** SOSYAL MEDYA BAŞLANGIÇ *********/
function saveSocialLink($facebook,$instagram,$googleplus,$twitter,$linkedin){
    $query = "UPDATE social SET facebook = '$facebook',instagram = '$instagram',googleplus = '$googleplus',twitter = '$twitter',linkedin = '$linkedin' WHERE id = 1";
    run($query);
}

function getSocialLinks(){
    $query = "Select * from social where id=1";
    $result = run($query);
    $row = $result->fetch_assoc();
    return $row;
}
/******** SOSYAL MEDYA BİTİŞ *********/

/******** HAKKIMIZDA BAŞLANGIÇ *********/
function saveAboutUs($hakkımızda,$misyon,$vizyon){
    $query = "UPDATE aboutus SET about = '$hakkımızda',misyon = '$misyon',vizyon = '$vizyon' WHERE id = 1";
    run($query);
}

function getAboutUs(){
    $query = "Select * from aboutus where id=1";
    $result = run($query);
    $row = $result->fetch_assoc();
    return $row;
}
/******** HAKKIMIZDA BİTİŞ *********/

/******** İLETİŞİM BAŞLANGIÇ *********/
function saveContactInfo($tel1,$tel2,$gsm,$email,$map,$adres){
    $query = "UPDATE contact SET phone = '$tel1',phone2 = '$tel2',gsm = '$gsm',email='$email',maplink='$map',adress='$adres' WHERE id = 1";
    run($query);
}

function getContactInfo(){
    $query = "Select * from contact where id=1";
    $result = run($query);
    $row = $result->fetch_assoc();
    return $row;
}
/******** İLETİŞİM BİTİŞ *********/

/******** FİRMA BAŞLANGIÇ *********/
function saveCompanyInfo($name){
    $logo=fileUpload("../assets/images");
    if($logo!="null") {
        $query = "UPDATE general SET companyName = '$name',companyLogo = '$logo' WHERE id = 1";
    }else{
        $query = "UPDATE general SET companyName = '$name' WHERE id = 1";
    }
    run($query);
}

function getCompanyInfo(){
    $query = "Select * from general where id=1";
    $result = run($query);
    $row = $result->fetch_assoc();
    return $row;
}
/******** FİRMA BİTİŞ *********/

/******** GALERİ BAŞLANGIÇ *********/
function saveGalleryImage(){
    foreach($_FILES["files"]["tmp_name"] as $key=>$tmp_name)
    {
        $path=$_FILES["files"]["name"][$key];
        $source=$_FILES["files"]["tmp_name"][$key];
        $path=date("Y-m-d_H:i:s")."_".$path;
        $save = move_uploaded_file($source, "../assets/images/gallery" . "/" . $path);
        if($save){
            $result=$path;
            $query = "INSERT INTO gallery (source) VALUES ('$result')";
            run($query);
        }
    }
}

function getGalleryImages(){
    $query = "Select * from gallery";
    $result = run($query);
    return $result;
}

function deleteImageFromGallery($id){
    $query = "Select * from gallery where id={$id}";
    $result = run($query);
    $row = $result->fetch_assoc();
    unlink("../assets/images/gallery/".$row['source']);
    $query = "delete from gallery where id=$id";
    run($query);
    done("./pages/gallery.php");
}
/******** GALERİ BİTİŞ *********/

/******** KULLANICILAR BAŞLANGIÇ *********/
function saveNewUser($email,$pass){
    $encPass=md5($pass);
    $query = "INSERT INTO admins (email,password) VALUES ('$email','$encPass')";
    run($query);
}

function getRegisteredUsers(){
    $query = "Select * from admins";
    $result = run($query);
    return $result;
}

function getUserName(){
    $email=$_SESSION['UserID'];
    $query = "select * from admins where email='$email'";
    $result=run($query);
    $row = $result->fetch_assoc();
    return $row['name'].' '.$row['surname'];
}

function getUserImage(){
    $email=$_SESSION['UserID'];
    $query = "select * from admins where email='$email'";
    $result=run($query);
    $row = $result->fetch_assoc();
    return $row['picture'];
}

function changeUserPass($pass){
    $encPass=md5($pass);
    $email=$_SESSION['UserID'];
    $query = "UPDATE admins SET password = '$encPass' WHERE email = '$email'";
    run($query);
}
/******** KULLANICILAR BİTİŞ *********/

/********HİZMETLER  BAŞLANGIÇ********/
function addService($name,$content,$icon,$keywords,$title,$description,$image_alt){
    $slug=slugify($name);
    $service_status=1;
    $image=fileUpload("../assets/images/services");
    $query = "INSERT INTO services (slug,name,icon,content,title,description,image,image_alt,status) VALUES ('$slug','$name','$icon','$content','$title','$description','$image','$image_alt',$service_status)";
    $conn = connect();
    $result = mysqli_query($conn, $query);
    $last_id = mysqli_insert_id($conn);
    $conn->close();
    addServiceKeywords($last_id,$keywords);
    done("pages/listservice.php");
}

function addServiceKeywords($service_id,$keywords){
    foreach ($keywords as $key) {
        $keyword_id=0;
        if(checkKeywordExist($key)){
            $keyword_id=getKeywordId($key);
        }else{
            $keyword_id=addNewKeyword($key);
        }
        $query = "INSERT INTO service_keywords (keyword_id,service_id) VALUES ($keyword_id,$service_id)";
        run($query);
    }
}

function getAllServices(){
    $query = "Select * from services";
    $result = run($query);
    return $result;
}

function getServiceKeywords($id){
    $query = "Select * from service_keywords inner join keywords on keywords.id=service_keywords.keyword_id where service_id=$id";
    $result = run($query);
    return $result;
}

function disableService($id){
    $query = "UPDATE services SET status = 0 WHERE id = $id";
    run($query);
    header("Location: ../pages/listservice.php");
}

function enableService($id){
    $query = "UPDATE services SET status = 1 WHERE id = $id";
    run($query);
    header("Location:../pages/listservice.php");
}

function deleteService($id){
  $list=getService($id);
  $row = $list->fetch_assoc();
  deleteOldServiceImage($row["image"]);
  deleteOldServiceKeywords($id);
  $query = "DELETE FROM services where id=$id";
  $result = run($query);
  header("Location:../pages/listservice.php");
}

function getService($id){
    $query = "Select * from services where id=$id";
    $result=run($query);
    return $result;
}

function deleteOldServiceImage($oldImage){
  if($oldImage!="null"){
    unlink("../assets/images/services/".$oldImage);
  }
}

function deleteOldServiceKeywords($id){
    $query = "DELETE FROM service_keywords where service_id=$id";
    $result = run($query);
}

function updateService($name,$content,$icon,$keywords,$title,$description,$image_alt,$id,$oldImage){
    $slug=slugify($name);
    $image=fileUpload("../assets/images/services");
    deleteOldServiceKeywords($id);
    addServiceKeywords($id,$keywords);
    if($image!="null") {
        deleteOldServiceImage($oldImage);
        $query = "UPDATE services SET slug = '$slug',name='$name',title='$title',content='$content',icon='$icon',description='$description',image_alt='$image_alt',image='$image' WHERE id = $id";
    }else{
        $query = "UPDATE services SET slug = '$slug',name='$name',title='$title',content='$content',icon='$icon',description='$description',image_alt='$image_alt' WHERE id = $id";
    }
    run($query);
    done("pages/listservice.php");
}

/********HİZMETLER BİTİŞ***********/

/********BLOGLAR  BAŞLANGIÇ********/
function addNewBlog($post_title,$post_content,$post_description,$post_image_alt,$keywords){
    $post_slug=slugify($post_title);
    $post_date=date("Y-m-d H:i:s");
    $post_image=fileUpload("../assets/images/blogs");
    $post_status=1;
    $post_views=0;
    $query = "INSERT INTO blogs (post_title, post_slug, post_content, post_description,post_image_alt,post_date,post_status,post_views,post_image,post_update)VALUES ('$post_title','$post_slug','$post_content','$post_description','$post_image_alt','$post_date',$post_status,$post_views,'$post_image','$post_date')";
    $conn = connect();
    $result = mysqli_query($conn, $query);
    $last_id = mysqli_insert_id($conn);
    $conn->close();
    addBlogKeywords($last_id,$keywords);
    done("pages/listblog.php");
}

function addBlogKeywords($blog_id,$keywords){
    foreach ($keywords as $key) {
        $keyword_id=0;
        if(checkKeywordExist($key)){
            $keyword_id=getKeywordId($key);
        }else{
            $keyword_id=addNewKeyword($key);
        }
        $query = "INSERT INTO post_keywords (keyword_id,post_id) VALUES ($keyword_id,$blog_id)";
        run($query);
    }
}

function getAllBlogs(){
    $query = "Select * from blogs";
    $result = run($query);
    return $result;
}

function getBlogKeywords($id){
    $query = "Select * from post_keywords inner join keywords on keywords.id=post_keywords.keyword_id where post_id=$id";
    $result = run($query);
    return $result;
}

function disableBlog($id){
    $query = "UPDATE blogs SET post_status = 0 WHERE id = $id";
    run($query);
    header("Location: ../pages/listblog.php");
}

function enableBlog($id){
    $query = "UPDATE blogs SET post_status = 1 WHERE id = $id";
    run($query);
    header("Location:../pages/listblog.php");
}

function deleteBlog($id){
  $list=getBlog($id);
  $row = $list->fetch_assoc();
  deleteOldBlogImage($row["post_image"]);
  deleteOldBlogKeywords($id);
  $query = "DELETE FROM blogs where id=$id";
  $result = run($query);
  header("Location:../pages/listblog.php");
}

function getBlog($id){
    $query = "Select * from blogs where id=$id";
    $result = run($query);
    return $result;
}

function deleteOldBlogImage($oldImage){
  if($oldImage!="null"){
    unlink("../assets/images/blogs/".$oldImage);
  }
}

function deleteOldBlogKeywords($id){
    $query = "DELETE FROM post_keywords where post_id=$id";
    $result = run($query);
}

function updateBlog($post_title,$post_content,$post_description,$post_image_alt,$id,$oldImage,$keywords){
    $post_slug=slugify($post_title);
    $post_date=date("Y-m-d H:i:s");
    deleteOldBlogImage($oldImage);
    $post_image=fileUpload("../assets/images/blogs");
    deleteOldBlogKeywords($id);
    addBlogKeywords($id,$keywords);
    if($post_image!="null") {
        deleteOldBlogImage($oldImage);
        $query = "UPDATE blogs SET post_title = '$post_title',post_slug='$post_slug',post_content='$post_content',post_description='$post_description',post_image_alt='$post_image_alt',
        post_update='$post_date',post_image='$post_image' WHERE id = $id";
    }else{
      $query = "UPDATE blogs SET post_title = '$post_title',post_slug='$post_slug',post_content='$post_content',post_description='$post_description',post_image_alt='$post_image_alt',
      post_update='$post_date' WHERE id = $id";
    }
    run($query);
    done("pages/listblog.php");
}

/********BLOGLAR  BİTİŞ********/

/********** Keywords başlangıç *******/

function addNewKeyword($key){
    $query = "INSERT INTO keywords (keyword) VALUES ('$key')";
    $conn = connect();
    mysqli_query($conn,"SET NAMES UTF8");
    $result = mysqli_query($conn, $query);
    $last_id = mysqli_insert_id($conn);
    return $last_id;
}

function getKeywordId($keyword){
    $query = "SELECT * FROM keywords WHERE keyword='$keyword'";
    $result = run($query);
    $row = $result->fetch_assoc();
    return $row["id"];
}

function checkKeywordExist($keyword){
    $query = "SELECT * FROM keywords WHERE keyword='$keyword'";
    $result = run($query);
    if (mysqli_num_rows($result)) {
        return true;
    } else {
        return false;
    }
}

function echoAllKeyWords(){
    $query = "SELECT * FROM keywords";
    $result = run($query);
    $keywordArray="[";
    while($row=$result->fetch_assoc()){
        $keyword=$row["keyword"];
        $keywordArray=$keywordArray."'".$keyword."',";
    }
    $keywordArray=rtrim($keywordArray,", ");
    $keywordArray=$keywordArray."]";
    echo $keywordArray;
}

/********** Keywords bitiş *******/
?>
