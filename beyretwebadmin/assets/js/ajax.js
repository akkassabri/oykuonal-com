$(document).ready(function () {
    $("#page_sec_select").bind("change keyup", function () {
        swal("dfdf");
        var id = $(this).val();
        $.ajax({
            url: "src/main.php", data: {"page_id": id}, type: "POST", success: function (response) {
                var page = JSON.parse(response);
                $('#page_title').val(page.page_title);
                $('#page_description').val(page.page_description);
                $('#page_keywords').val(page.page_keywords);
            }
        });
    });

    $(".sidebar-collapse ul li a").each(function () {
        if ((window.location.pathname).includes($(this).attr("href"))) $(this).addClass("active-menu");
    });
    $("#tercih_form").on('submit', function (e) {
        e.preventDefault();
        CKEDITOR.instances['tercih_yazi'].updateElement();
        $.ajax({
            url: "src/main.php",
            data: {"tercih": $('#tercih_yazi').val()},
            type: "POST",
            success: function (response) {
                var r = JSON.parse(response);
                swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
            }
        });
    });
    $("#anasayfa_form").on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: "src/main.php", data: $(this).serialize(), type: "POST", success: function (response) {
                var r = JSON.parse(response);
                swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
            }
        });
    });
    $("#referans_delete").on('click', function () {
        var id = $('#referans_sec_select').val();
        if (id == null) swal('Lütfen sol üstten silmek istediğiniz referansı seçin'); else {
            swal({
                title: "Emin misiniz ?",
                text: "Siteniz de gözüken bu referansı silmek istediğinize emin misiniz?",
                buttons: ["İptal", "Evet"],
                icon: "info",
            }).then((isConfirm) => {
                if (isConfirm === true) {
                    $.ajax({
                        url: "src/main.php",
                        data: {"referans_delete_id": id},
                        type: "POST",
                        success: function (response) {
                            var r = JSON.parse(response);
                            swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
                        }
                    });
                }
            });
        }
    });
    $("#referans_sec_select").bind("change keyup", function () {
        var id = $(this).val();
        $.ajax({
            url: "src/main.php", data: {"referans_id": id}, type: "POST", success: function (response) {
                var referans = JSON.parse(response);
                $('#referansD_ad').val(referans.referans_ad);
                $('#referansD_aciklama').val(referans.referans_aciklama);
                $('#referansD_url').val(referans.referans_url);
                $('#referansD_hizmet').val(referans.referans_hizmetleri);
            }
        });
    });
    $("#page_edit_form").on('submit', function (e) {
        e.preventDefault();
        swal("vd");
        var id = $('#page_sec_select').val();
        var description = $('#page_description').val();
        var title = $('#page_title').val();
        var keywords = $('#page_keywords').val();
        if (id == null) swal('Lütfen sol üstten güncellemek istediğiniz sayfayı seçin'); else {
            swal({
                title: "Emin misiniz ?",
                text: "Siteniz de gözüken bu sayfanın ayarlarını değiştirmek istediğinize emin misiniz?",
                buttons: ["İptal", "Evet"],
                icon: "info",
            }).then((isConfirm) => {
                if (isConfirm === true) {
                    $.ajax({
                        url: "src/main.php",
                        data: {
                            "pages_id": id,
                            "page_keywords": keywords,
                            "page_description": description,
                            "page_title": title
                        },
                        type: "POST",
                        success: function (response) {
                            var r = JSON.parse(response);
                            swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
                        }
                    });
                }
            });
        }
    });
    $("#referans_duzenle_form").on('submit', function (e) {
        e.preventDefault();
        var id = $('#referans_sec_select').val();
        var ad = $('#referansD_ad').val();
        var aciklama = $('#referansD_aciklama').val();
        var url = $('#referansD_url').val();
        var hizmet = $('#referansD_hizmet').val();
        if (id == null) swal('Lütfen sol üstten silmek istediğiniz hizmeti seçin'); else {
            swal({
                title: "Emin misiniz ?",
                text: "Siteniz de gözüken bu referansı değiştirmek istediğinize emin misiniz?",
                buttons: ["İptal", "Evet"],
                icon: "info",
            }).then((isConfirm) => {
                if (isConfirm === true) {
                    $.ajax({
                        url: "src/main.php",
                        data: {
                            "referans_update_id": id,
                            "referans_update_ad": ad,
                            "referans_update_aciklama": aciklama,
                            "referans_update_url": url,
                            "referans_update_hizmet": hizmet,
                        },
                        type: "POST",
                        success: function (response) {
                            var r = JSON.parse(response);
                            swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
                        }
                    });
                }
            });
        }
    });
    $("#hizmet_sec_select").bind("change keyup", function () {
        var id = $(this).val();
        $.ajax({
            url: "src/main.php", data: {"hizmet_id": id}, type: "POST", success: function (response) {
                var hizmet = JSON.parse(response);
                $('#hizmetD_ad').val(hizmet.hizmet_ad);
                $('#hizmetD_aciklama').val(hizmet.hizmet_aciklama);
                $('#hizmetD_icon').val(hizmet.hizmet_icon);
                $('#hizmetD_title').val(hizmet.hizmet_title);
                $('#hizmetD_keywords').val(hizmet.hizmet_keywords);
                $('#hizmetD_description').val(hizmet.hizmet_description);
                $('#hizmetD_resim_alt').val(hizmet.hizmet_resim_alt);
            }
        });
    });
    $('#kurumsal_form').on('submit', function (e) {
        e.preventDefault();
        CKEDITOR.instances['kurumsal_yazi'].updateElement();
        $.ajax({
            url: "src/main.php", data: $(this).serialize(), type: "POST", success: function (response) {
                var r = JSON.parse(response);
                swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
            }
        });
    });
    $('#referans_ekle_form').on('submit', function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: "src/main.php",
            data: data,
            contentType: false,
            processData: false,
            type: "POST",
            success: function (response) {
                var r = JSON.parse(response);
                swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
            }
        });
    });
    $('#slider_ekle_form').on('submit', function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: "src/main.php",
            data: data,
            contentType: false,
            processData: false,
            type: "POST",
            success: function (response) {
                var r = JSON.parse(response);
                swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
            }
        });
    });
    $('#hizmet_duzenle_form').on('submit', function (e) {
        e.preventDefault();
        var id = $("#hizmet_sec_select").val();
        var ad = $("#hizmetD_ad").val();
        var aciklama = $("#hizmetD_aciklama").val();
        var icon = $("#hizmetD_icon").val();
        var title = $("#hizmetD_title").val();
        var description = $("#hizmetD_description").val();
        var keywords = $("#hizmetD_keywords").val();
        if (id == null) swal('Lütfen sol üstten silmek istediğiniz hizmeti seçin'); else {
            swal({
                title: "Emin misiniz ?",
                text: "Siteniz de gözüken bu hizmeti değiştirmek istediğinize emin misiniz?",
                buttons: ["İptal", "Evet"],
                icon: "info",
            }).then((isConfirm) => {
                if (isConfirm === true) {
                    $.ajax({
                        url: "src/main.php",
                        data: {
                            "hizmet_update_id": id,
                            "hizmet_update_ad": ad,
                            "hizmet_update_aciklama": aciklama,
                            "hizmet_update_icon": icon,
                            "hizmet_update_title": title,
                            "hizmet_update_description": description,
                            "hizmet_update_keywords": keywords
                        },
                        type: "POST",
                        success: function (response) {
                            var r = JSON.parse(response);
                            swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
                        }
                    });
                }
            });
        }
    });
    $('#hizmet_delete').on('click', function () {
        var id = $("#hizmet_sec_select").val();
        if (id == null) swal('Lütfen sol üstten silmek istediğiniz hizmeti seçin'); else {
            swal({
                title: "Emin misiniz ?",
                text: "Siteniz de gözüken bu hizmeti silmek istediğinize emin misiniz?",
                buttons: ["İptal", "Evet"],
                icon: "info",
            }).then((isConfirm) => {
                if (isConfirm === true) {
                    $.ajax({
                        url: "src/main.php",
                        data: {"hizmet_delete_id": id},
                        type: "POST",
                        success: function (response) {
                            var r = JSON.parse(response);
                            swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
                        }
                    });
                }
            });
        }
    });
    $('#hizmet_ekle_form').on('submit', function (e) {
        e.preventDefault();
        var data = new FormData(this);

        $.ajax({
            url: "src/main.php",
            data: data,
            contentType: false,
            processData: false,
            type: "POST",
            success: function (response) {
                var r = JSON.parse(response);
                swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
            }
        });
    });
    $('#iletisim_form').on('submit', function (e) {
        e.preventDefault();
        swal({
            title: "Emin misiniz ?",
            text: "İletişim bilgilerinizi değiştirmek istediğinize emin misiniz?",
            buttons: ["İptal", "Evet"],
            icon: "info",
        }).then((isConfirm) => {
            if (isConfirm === true) {
                $.ajax({
                    url: "src/main.php",
                    data: $('#iletisim_form').serialize(),
                    type: "POST",
                    success: function (response) {
                        var r = JSON.parse(response);
                        swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
                    }
                });
            }
        });
    });

    $('#slider_form').on('submit', function (e) {
        e.preventDefault();

        $.ajax({
            url: "src/main.php",
            data: $(this).serialize(),
            type: "POST",
            success: function (response) {
                var r = JSON.parse(response);
                swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
            }
        });
    });

    $("#kisi_ekle_form").on('submit', function (e) {
       e.preventDefault();
       var data = new FormData(this);
        $.ajax({
            url: "src/main.php",
            contentType: false,
            processData: false,
            data: data,
            type: "POST",
            success: function (response) {
                var r = JSON.parse(response);
                swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
            }
        });
    });

    $("tr").on("click", "#delete_slider", function () {
        var id = $(this).closest('tr').attr('id');
        $.ajax({
            url: "src/main.php",
            data: {"delete_slider_id" : id},
            type: "POST",
            success: function (response) {
                var r = JSON.parse(response);
                swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
            }
        });
    });

    $("tr").on("click", "#edit_kisi", function () {

        var id = $(this).closest('tr').attr('id');
        window.location.href = 'pages/calisanlar.php?id=' + id;

    });

    $("tr").on("click", "#edit_post", function () {

        var id = $(this).closest('tr').attr('id');
        window.location.href = 'pages/post-edit.php?id=' + id;

    });

    $("tr").on("click", "#publish_post", function () {

        var id = $(this).closest('tr').attr('id');
        $.ajax({
            url: "src/main.php",
            data: {"publish_post" : id},
            type: "POST",
            success: function (response) {
                var r = JSON.parse(response);
                swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
            }
        });

    });

    $("tr").on("click", "#not_publish_post", function () {

        var id = $(this).closest('tr').attr('id');
        $.ajax({
            url: "src/main.php",
            data: {"not_publish_post" : id},
            type: "POST",
            success: function (response) {
                var r = JSON.parse(response);
                swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
            }
        });

    });

    $("tr").on("click", "#delete_post", function () {

        var id = $(this).closest('tr').attr('id');
        $.ajax({
            url: "src/main.php",
            data: {"delete_post" : id},
            type: "POST",
            success: function (response) {
                var r = JSON.parse(response);
                swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
            }
        });

    });

    $("tr").on("click", "#delete_kisi", function () {

        var id = $(this).closest('tr').attr('id');
        $.ajax({
            url: "src/main.php",
            data: {"delete_kisi" : id},
            type: "POST",
            success: function (response) {
                var r = JSON.parse(response);
                swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
            }
        });

    });

    $("#kisi_duzenle_form").on('submit', function (e) {
       e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: "src/main.php",
            data: data,
            contentType: false,
            processData: false,
            type: "POST",
            success: function (response) {
                var r = JSON.parse(response);
                swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
            }
        });

    });

    $("#post_edit_form").on('submit', function (e) {
        e.preventDefault();
        CKEDITOR.instances['postD_icerik'].updateElement();
        var data = new FormData(this);
        $.ajax({
            url: "src/main.php",
            data: data,
            contentType: false,
            processData: false,
            type: "POST",
            success: function (response) {
                var r = JSON.parse(response);
                swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
            }
        });

    });

    $("#home_page_background").on('submit', function (e) {
        e.preventDefault();

        var data = new FormData(this);

        $.ajax({
            url: "src/main.php",
            data: data,
            contentType: false,
            processData: false,
            type: "POST",
            success: function (response) {
                var r = JSON.parse(response);
                swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
            }
        });
    });

    $('#post_ekle_form').on('submit', function (e) {
        e.preventDefault();
        CKEDITOR.instances['post_icerik'].updateElement();

        var data = new FormData(this);

        $.ajax({
            url: "src/main.php",
            data: data,
            contentType: false,
            processData: false,
            type: "POST",
            success: function (response) {

                var r = JSON.parse(response);
                swal({title: "İşlem Sonucu", icon: r.type, text: r.message, button: "Tamam"});
            }
        });
    });
});
CKEDITOR.replace('post_icerik');
CKEDITOR.replace('kurumsal_yazi');
CKEDITOR.replace('tercih_yazi');