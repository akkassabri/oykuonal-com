<?php
if(!isset($_SESSION)){
    session_start();
}
if(!isset($_SESSION['UserID'])){
    header("location:../login.php");
}
?>
<?php
require_once '../functions/backend.php';
checkAccess(basename(__FILE__));
$company=getCompanyInfo();

if (isset($_POST['submit'])) {
    $name=$_POST['isim'];
    saveCompanyInfo($name);
    echo "<meta http-equiv='refresh' content='0'>";
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include '../includes/head.php'; ?>
	</head>
	<body class="menubar-left menubar-unfold menubar-light theme-primary">
		<?php include '../includes/header.php'; ?>
		<?php include '../includes/leftmenu.php'; ?>

		<main id="app-main" class="app-main">
		  <div class="wrap">
		    <section class="app-content">
		    	<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">Firma Bilgileri</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body">
                        <div class="row">
                            <div class="col-md-12">
                                <img style="margin-left:auto; margin-right:auto; display:block;" src="../assets/images/<?php echo $company['companyLogo']?>" alt="Resim Görüntülenemiyor" height="300" width="300">
                            </div>
                        </div>
                        <hr class="widget-separator">
						<form class="form-horizontal" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
							<div class="form-group">
								<label for="isim" class="col-sm-3 control-label">Firma İsmi</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" name="isim" placeholder="Firma ismi" value="<?php echo $company['companyName']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label for="logo" class="col-sm-3 control-label">Firma Logosu</label>
								<div class="col-sm-9">
									<input type="file" class="form-control" name="img">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-9 col-sm-offset-3">
									<button type="submit" name="submit" class="btn btn-success">Kaydet</button>
								</div>
							</div>
						</form>
					</div><!-- .widget-body -->
				</div><!-- .widget -->
		    </section><!-- #dash-content -->
		  </div>
		  <?php include '../includes/footer.php'; ?>
		</main>
		<?php include("../includes/foot.php") ?>
	</body>
</html>
