<?php
if(!isset($_SESSION)){
    session_start();
}
if(!isset($_SESSION['UserID'])){
    header("location:../login.php");
}
?>
<?php
require_once '../functions/backend.php';
if (isset($_POST['submit'])) {
	$service_title=$_POST['service_title'];
  $service_icon=$_POST['service_icon'];
	$service_content=$_POST['service_content'];
	$service_description=$_POST['service_description'];
	$service_image_alt=$_POST['service_image_alt'];
	$service_keywords=$_POST['tages'];
  $id=$_POST['post_id'];
  $old_image=$_POST['old_image'];
  updateService($service_title,$service_content,$service_icon,$service_keywords,$service_title,$service_description,$service_image_alt,$id,$old_image);
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include '../includes/head.php'; ?>
		<link href="../assets/css/bootstrap-fileupload.min.css" rel="stylesheet" />
		<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">
		<link href="../assets/css/jquery.tagit.css" rel="stylesheet" type="text/css">
	</head>
	<body class="menubar-left menubar-unfold menubar-light theme-primary">
		<?php include '../includes/header.php'; ?>
		<?php include '../includes/leftmenu.php'; ?>
		<main id="app-main" class="app-main">
		    <section class="app-content">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
                                Hizmet Düzenleme
                            </div>
							<div class="panel-body">
								<form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
                  <input name="post_id" value="<?php $list=getService($_GET['id']);$row = $list->fetch_assoc();echo $row["id"];?>" hidden>
                  <input name="old_image" value="<?php echo $row["image"];?>" hidden>
									<input name="post_ekle_formu" value="" hidden>
									<div class="form-group">
										<label>Hizmet Başlık </label>
										<input name="service_title" class="form-control" type="text" value="<?php echo $row["name"];?>">
									</div>
                  <div class="form-group">
										<label>Hizmet İkonu </label>
										<input name="service_icon" class="form-control" type="text" value="<?php echo $row["icon"];?>">
									</div>
									<div class="form-group">
										<label>Hizmet İçeriği</label>
										<textarea id="post_icerik" name="service_content"><?php echo $row["content"];?></textarea>
									</div>
									<div class="form-group">
										<label>Sayfa Açıklaması</label>
										<input class="form-control" cols="5" name="service_description" value="<?php echo $row["description"];?>" ></input>
									</div>
									<div class="form-group">
										<label>Anahtar Kelimeleri</label>
                    <ul id="keywords" name="service_keywords">
                      <?php
                                    $list2 = getServiceKeywords($_GET['id']);
                                    while ($row2 = $list2->fetch_assoc()) {
                                        echo '<li>' . $row2["keyword"] . '</li>';
                                    }
                                  ?>
                    </ul>
									</div>
									<div class="form-group">
										<label>Fotoğraf Yükle</label>
										<div class="">
											<div class="fileupload fileupload-new" data-provides="fileupload">
												<div class="fileupload-preview thumbnail"
													style="width: 200px; height: 150px;">
													<img src="../assets/images/services/<?php echo $row["image"];?>">
												</div>
												<div>
													<span class="btn btn-file btn-success"><span class="fileupload-new">Resim Seç</span><span
														class="fileupload-exists">Değiştir</span><input
														name="img" type="file"></span>
													<a href="#" class="btn btn-danger fileupload-exists"
														data-dismiss="fileupload">Kaldır</a>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label>Hizmet Resimi Açıklaması</label>
										<input name="service_image_alt" class="form-control" type="text" value="<?php echo $row["image_alt"];?>">
									</div>
									<button type="submit" name="submit" class="btn btn-success btn-block">Kaydet</button>
								</form>
							</div>
						</div>
					</div>
				</div>
		    </section><!-- #dash-content -->
		  <?php include '../includes/footer.php'; ?>
		</main>
		<?php include("../includes/foot.php") ?>
		<script type="text/javascript">
    var sampleTags =<?php echoAllKeyWords(); ?>;
        		$(document).ready(function() {
            		$("#keywords").tagit({
            			 availableTags: sampleTags,
            			allowSpaces: true,
            			fieldName: "tages[]"
            		});
        		});
		</script>
		<script src="../assets/js/bootstrap-fileupload.js"></script><!-- CUSTOM SCRIPTS -->
		<script src="../assets/js/ajax.js"></script>
		<script src="../assets/js/tag-it.js" type="text/javascript" charset="utf-8"></script>
	</body>
</html>
