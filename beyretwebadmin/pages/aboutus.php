<?php
if(!isset($_SESSION)){
    session_start();
}
if(!isset($_SESSION['UserID'])){
    header("location:../login.php");
}
?>
<?php
require_once '../functions/backend.php';
checkAccess(basename(__FILE__));
$aboutUs=getAboutUs();
if (isset($_POST['submit'])) {
    $hakkımızda=$_POST['hakkımızda'];
    $misyon=$_POST['misyon'];
    $vizyon=$_POST['vizyon'];
    saveAboutUs($hakkımızda,$misyon,$vizyon);
    echo "<meta http-equiv='refresh' content='0'>";
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include '../includes/head.php'; ?>
    		<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">
	</head>
	<body class="menubar-left menubar-unfold menubar-light theme-primary">
		<?php include '../includes/header.php'; ?>
		<?php include '../includes/leftmenu.php'; ?>
		<main id="app-main" class="app-main">
		  <div class="wrap">
		    <section class="app-content">
		    	<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">Hakkımızda Sayfası</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body">
						<form class="form-horizontal" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
							<div class="form-group">
								<label for="hakkımızda" class="col-sm-3 control-label">Hakkımızda:</label>
								<div class="col-sm-9">
									<textarea id="post_icerik" name="hakkımızda" placeholder="Hakkımızda..."><?php echo $aboutUs['about']; ?></textarea>
								</div>
							</div>
							<div class="form-group" hidden>
								<label for="misyon" class="col-sm-3 control-label">Misyon:</label>
								<div class="col-sm-9">
									<textarea id="post_icerik" name="misyon" placeholder="Misyon..."><?php echo $aboutUs['misyon']; ?></textarea>
								</div>
							</div>
							<div class="form-group" hidden>
								<label for="vizyon" class="col-sm-3 control-label">Vizyon:</label>
								<div class="col-sm-9">
									<textarea class="form-control" name="vizyon" placeholder="Vizyon..."><?php echo $aboutUs['vizyon']; ?></textarea>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-9 col-sm-offset-3">
									<button type="submit" name="submit" class="btn btn-success">Kaydet</button>
								</div>
							</div>
						</form>
					</div>
				</div>
		    </section>
		  </div>
		  <?php include '../includes/footer.php'; ?>
		</main>
		<?php include("../includes/foot.php") ?>
    <script src="../assets/js/ajax.js"></script>
	</body>
</html>
