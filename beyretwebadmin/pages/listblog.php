<?php
if(!isset($_SESSION)){
    session_start();
}
if(!isset($_SESSION['UserID'])){
    header("location:../login.php");
}
?>
<?php
require_once '../functions/backend.php';
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include '../includes/head.php'; ?>
		<script src="../libs/bower/jquery/dist/jquery.js"></script>
	</head>
	<body class="menubar-left menubar-unfold menubar-light theme-primary">
		<?php include '../includes/header.php'; ?>
		<?php include '../includes/leftmenu.php'; ?>
		<main id="app-main" class="app-main">
		  <div class="wrap">
		    <section class="app-content">
            	<div class="row">
                	<div class="col-md-12">
                    	<div class="widget p-lg">
                        	<div class="row">
                            	<div class="col-md-8">
                                	<h4 class="m-b-lg">Tüm Blog Yazıları</h4>
                            	</div>
                        	</div>
                        	<table class="table table-hover">
                            	<tr>
                                	<th>#</th>
                                	<th>Blog Başlığı</th>
                                	<th>Görüntülenme Sayısı</th>
                                	<th>Eklenme Tarihi</th>
                                	<th>Son Güncelle Tarihi</th>
                                	<th>Ayarlar</th>
                            	</tr>
                            	<?php
                            	$list = getAllBlogs();
	                            	while ($row = $list->fetch_assoc()) {
	                                //echo "id: " . $row["pid"]. " - Name: " . $row["name"]. " " . $row["price"]. "<br>";
	                                	echo '<tr>';
	                                	echo '<td>' . $row["id"] . '</td>';
	                                	echo '<td>' . $row["post_title"] . '</td>';
	                                	echo '<td>' . $row["post_views"] . '</td>';
	                                	echo '<td>' . $row["post_date"] . '</td>';
	                                	echo '<td>' . $row["post_update"] . '</td>';
	                                	if($row["post_status"]=="1"){
											echo '<td>
											<a href="updateblog.php?id=' . $row['id'] . '"><button type="button" class="btn btn-warning"><i class="fa fa-cog"></i> Düzenle</button></a>
											<a href="../functions/backend.php?disableBlog_id=' . $row['id'] . '"><button type="button" class="btn btn-primary"><i class="fa fa-thumbs-down"></i> Yayından Kaldır</button></a>
                      </td>';
	                                	}else{
											echo '<td>
											<a href="updateblog.php?id=' . $row['id'] . '"><button type="button" class="btn btn-warning"><i class="fa fa-cog"></i> Düzenle</button></a>
											<a href="../functions/backend.php?enableBlog_id=' . $row['id'] .'"><button type="button" class="btn btn-success"><i class="fa fa-thumbs-up"></i> Yayınla</button></a>
                      <a href="../functions/backend.php?deleteBlog_id=' . $row['id'] . '"><button type="button" class="btn btn-danger"><i class="fa fa-remove"></i>Sil</button></a>
                    	</td>';
	                                	}

	                                	echo '</tr>';
                            		}
                            	?>
                        	</table>
                    	</div><!-- .widget -->
                	</div><!-- END column -->
            	</div>
		    </section><!-- #dash-content -->
		  </div>
		  <?php include '../includes/footer.php'; ?>
		</main>
		<?php include("../includes/foot.php") ?>
	</body>
</html>
