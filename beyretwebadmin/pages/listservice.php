<?php
if(!isset($_SESSION)){
    session_start();
}
if(!isset($_SESSION['UserID'])){
    header("location:../login.php");
}
?>
<?php
require_once '../functions/backend.php';
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include '../includes/head.php'; ?>
		<script src="../libs/bower/jquery/dist/jquery.js"></script>
	</head>
	<body class="menubar-left menubar-unfold menubar-light theme-primary">
		<?php include '../includes/header.php'; ?>
		<?php include '../includes/leftmenu.php'; ?>
		<main id="app-main" class="app-main">
		  <div class="wrap">
		    <section class="app-content">
            	<div class="row">
                	<div class="col-md-12">
                    	<div class="widget p-lg">
                        	<div class="row">
                            	<div class="col-md-8">
                                	<h4 class="m-b-lg">Tüm Hizmetler</h4>
                            	</div>
                        	</div>
                        	<table class="table table-hover">
                            	<tr>
                                	<th>#</th>
                                	<th>Hizmet Başlığı</th>
                                	<th>Hizmet Açıklaması</th>
                                	<th>Ayarlar</th>
                            	</tr>
                            	<?php
                            	  $list = getAllServices();
	                            	while ($row = $list->fetch_assoc()) {
	                                //echo "id: " . $row["pid"]. " - Name: " . $row["name"]. " " . $row["price"]. "<br>";
	                                	echo '<tr>';
	                                	echo '<td>' . $row["id"] . '</td>';
	                                	echo '<td>' . $row["title"] . '</td>';
	                                	echo '<td>' . substr($row["description"],0,180) . '...</td>';
	                                	if($row["status"]=="1"){
											echo '<td>
											<a href="updateservice.php?id=' . $row['id'] . '"><button type="button" class="btn btn-warning"><i class="fa fa-cog"></i> Düzenle</button></a>
											<a href="../functions/backend.php?disableService_id=' . $row['id'] . '"><button type="button" class="btn btn-primary"><i class="fa fa-thumbs-down"></i> Yayından Kaldır</button></a>
                      </td>';
	                                	}else{
											echo '<td>
											<a href="updateservice.php?id=' . $row['id'] . '"><button type="button" class="btn btn-warning"><i class="fa fa-cog"></i> Düzenle</button></a>
											<a href="../functions/backend.php?enableService_id=' . $row['id'] .'"><button type="button" class="btn btn-success"><i class="fa fa-thumbs-up"></i> Yayınla</button></a>
                      <a href="../functions/backend.php?deleteService_id=' . $row['id'] . '"><button type="button" class="btn btn-danger"><i class="fa fa-remove"></i>Sil</button></a>
                    	</td>';
	                                	}

	                                	echo '</tr>';
                            		}
                            	?>
                        	</table>
                    	</div><!-- .widget -->
                	</div><!-- END column -->
            	</div>
		    </section><!-- #dash-content -->
		  </div>
		  <?php include '../includes/footer.php'; ?>
		</main>
		<?php include("../includes/foot.php") ?>
	</body>
</html>
