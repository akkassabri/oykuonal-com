<?php
if(!isset($_SESSION)){
    session_start();
}
if(!isset($_SESSION['UserID'])){
    header("location:../login.php");
}
?>
<?php
require_once '../functions/backend.php';
checkAccess(basename(__FILE__));
$socials=getSocialLinks();

if (isset($_POST['submit'])) {
  $facebook=$_POST['facebook'];
  $instagram=$_POST['instagram'];
  $googleplus=$_POST['googleplus'];
  $twitter=$_POST['twitter'];
  $linkedin=$_POST['linkedin'];
  saveSocialLink($facebook,$instagram,$googleplus,$twitter,$linkedin);
  echo "<meta http-equiv='refresh' content='0'>";
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include '../includes/head.php'; ?>
	</head>
	<body class="menubar-left menubar-unfold menubar-light theme-primary">
		<?php include '../includes/header.php'; ?>
		<?php include '../includes/leftmenu.php'; ?>

		<main id="app-main" class="app-main">
		  <div class="wrap">
		    <section class="app-content">
		    	<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">Sosyal Hesaplar</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body">
						<form class="form-horizontal" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
							<div class="form-group">
								<label class="col-sm-4 control-label">Facebook</label>
								<div class="col-sm-6">
									<input type="text" name="facebook" class="form-control" placeholder="Link giriniz" value="<?php echo $socials['facebook']; ?>">
								</div><!-- END column -->
							</div><!-- .form-group -->
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Instagram</label>
                                <div class="col-sm-6">
                                    <input type="text" name="instagram" class="form-control" placeholder="Link giriniz" value="<?php echo $socials['instagram']; ?>">
                                </div><!-- END column -->
                            </div><!-- .form-group -->
                            <div class="form-group">
                                <label class="col-sm-4 control-label">GooglePlus</label>
                                <div class="col-sm-6">
                                    <input type="text" name="googleplus" class="form-control" placeholder="Link giriniz" value="<?php echo $socials['googleplus']; ?>">
                                </div><!-- END column -->
                            </div><!-- .form-group -->
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Twitter</label>
                                <div class="col-sm-6">
                                    <input type="text" name="twitter" class="form-control" placeholder="Link giriniz" value="<?php echo $socials['twitter']; ?>">
                                </div><!-- END column -->
                            </div><!-- .form-group -->
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Linkedin</label>
                                <div class="col-sm-6">
                                    <input type="text" name="linkedin" class="form-control" placeholder="Link giriniz" value="<?php echo $socials['linkedin']; ?>">
                                </div><!-- END column -->
                            </div><!-- .form-group -->
                            <div class="row">
								<div class="col-sm-9 col-sm-offset-3">
									<button type="submit" name="submit" class="btn btn-success">Kaydet</button>
								</div>
							</div>
						</form>
					</div><!-- .widget-body -->
				</div><!-- .widget -->
		    </section><!-- #dash-content -->
		  </div>
		  <?php include '../includes/footer.php'; ?>
		</main>
		<?php include("../includes/foot.php") ?>
	</body>
</html>
