<?php
if(!isset($_SESSION)){
    session_start();
}
if(!isset($_SESSION['UserID'])){
    header("location:../login.php");
}
?>
<?php
require_once '../functions/backend.php';
if (isset($_POST['submit'])) {
	$post_title=$_POST['post_title'];
	$post_content=$_POST['post_content'];
	$post_description=$_POST['post_description'];
	$post_image_alt=$_POST['post_image_alt'];
	$post_keywords=$_POST['tages'];
  	addNewBlog($post_title,$post_content,$post_description,$post_image_alt,$post_keywords);
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include '../includes/head.php'; ?>
		<link href="../assets/css/bootstrap-fileupload.min.css" rel="stylesheet" />
		<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">
		<link href="../assets/css/jquery.tagit.css" rel="stylesheet" type="text/css">
	</head>
	<body class="menubar-left menubar-unfold menubar-light theme-primary">
		<?php include '../includes/header.php'; ?>
		<?php include '../includes/leftmenu.php'; ?>
		<main id="app-main" class="app-main">
		    <section class="app-content">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
                                Yeni bir post
                            </div>
							<div class="panel-body">
								<form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
									<input name="post_ekle_formu" value="" hidden>
									<div class="form-group">
										<label>Yazı Başlık </label>
										<input name="post_title" class="form-control" type="text">
									</div>
									<div class="form-group">
										<label>Yazı İçeriği</label>
										<textarea id="post_icerik" name="post_content"></textarea>
									</div>
									<div class="form-group">
										<label>Sayfa Açıklaması</label>
										<input class="form-control" cols="5" name="post_description"></input>
									</div>
									<div class="form-group">
										<label>Anahtar Kelimeleri</label>
										<ul id="keywords" name="post_keywords" class="form-control"></ul>
									</div>
									<div class="form-group">
										<label>Fotoğraf Yükle</label>
										<div class="">
											<div class="fileupload fileupload-new" data-provides="fileupload">
												<div class="fileupload-preview thumbnail"
													style="width: 200px; height: 150px;">
													<img src="">
												</div>
												<div>
													<span class="btn btn-file btn-success"><span class="fileupload-new">Resim Seç</span><span
														class="fileupload-exists">Değiştir</span><input
														name="img" type="file"></span>
													<a href="#" class="btn btn-danger fileupload-exists"
														data-dismiss="fileupload">Kaldır</a>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label>Yazı Resimi Açıklaması</label>
										<input name="post_image_alt" class="form-control" type="text">
									</div>
									<button type="submit" name="submit" class="btn btn-success btn-block">Kaydet</button>
								</form>
							</div>
						</div>
					</div>
				</div>
		    </section><!-- #dash-content -->
		  <?php include '../includes/footer.php'; ?>
		</main>
		<?php include("../includes/foot.php") ?>
		<script type="text/javascript">
    var sampleTags =<?php echoAllKeyWords(); ?>;
        		$(document).ready(function() {
            		$("#keywords").tagit({
            			 availableTags: sampleTags,
            			allowSpaces: true,
            			fieldName: "tages[]"
            		});
        		});
		</script>
		<script src="../assets/js/bootstrap-fileupload.js"></script><!-- CUSTOM SCRIPTS -->
		<script src="../assets/js/ajax.js"></script>
		<script src="../assets/js/tag-it.js" type="text/javascript" charset="utf-8"></script>
	</body>
</html>
