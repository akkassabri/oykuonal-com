<?php
session_start();
if (!isset($_SESSION['UserID'])) {
    header("location:login.php");
}
?>
<?php
require_once './functions/backend.php';
checkAccess(basename(__FILE__));
$products=getProducts();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include './includes/head.php'; ?>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary">
<?php include './includes/header.php'; ?>
<?php include './includes/leftmenu.php'; ?>

<main id="app-main" class="app-main">
    <div class="wrap">
        <section class="app-content">
            <!-- içerik buraya gelicek-->
            <div class="row">
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">Tüm Ürünler</h4>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <table class="table table-hover">
                                <tr>
                                    <th>Ürün Adı</th>
                                    <th>Ürün Fiyatı</th>
                                    <th>Ürün Açıklaması</th>
                                    <th>Ürün Resmi</th>
                                </tr>
                                <?php

                                while ($row = $products->fetch_assoc()) {
                                    //echo "id: " . $row["pid"]. " - Name: " . $row["name"]. " " . $row["price"]. "<br>";
                                    echo '<tr>';

                                    echo '<td>' . $row["name"] . '</td>';
                                    echo '<td>' . $row["price"] . '</td>';
                                    echo '<td>' . $row["info"] . '</td>';
                                    echo '<td><img src="'.$row["image"].'" alt="Resim Görüntülenemiyor" height="80" width="80"> </td>';
                                    echo '</tr>';

                                } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- #dash-content -->
    </div>
    <?php include './includes/footer.php'; ?>
</main>
<?php include("./includes/foot.php") ?>
</body>
</html>
