<?php
if(!isset($_SESSION)){
    session_start();
}
if(!isset($_SESSION['UserID'])){
    header("location:../login.php");
}
?>
<?php
require_once '../functions/backend.php';
checkAccess(basename(__FILE__));
$users=getRegisteredUsers();

if (isset($_POST['submit'])) {
    $email=$_POST['email'];
    $pass=$_POST['pass'];
    saveNewUser($email,$pass);
    echo "<meta http-equiv='refresh' content='0'>";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include '../includes/head.php'; ?>
</head>
<body class="menubar-left menubar-unfold menubar-light theme-primary">
<?php include '../includes/header.php'; ?>
<?php include '../includes/leftmenu.php'; ?>

<main id="app-main" class="app-main">
    <div class="wrap">
        <section class="app-content">
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title">Yeni Kullanıcı Ekleme</h4>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <form class="form-horizontal" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">Kullanıcı Emaili:</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" name="email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="map" class="col-sm-3 control-label">Kullanıcı Şifresi</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" name="pass">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="submit" name="submit" class="btn btn-success">Kaydet</button>
                            </div>
                        </div>
                    </form>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title">Kayıtlı Kullanıcılar</h4>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <?php
                    while ( $row = $users->fetch_assoc()){
                    ?>
                    <p><?php echo $row['email'];?></p>
                    <?php } ?>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </section><!-- #dash-content -->
    </div>
    <?php include '../includes/footer.php'; ?>
</main>

<?php include("../includes/foot.php") ?>
</body>
</html>
