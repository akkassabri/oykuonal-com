-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: oykuonalcom
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aboutus`
--

DROP TABLE IF EXISTS `aboutus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aboutus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `about` longtext COLLATE utf8_turkish_ci,
  `misyon` longtext COLLATE utf8_turkish_ci,
  `vizyon` longtext COLLATE utf8_turkish_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aboutus`
--

LOCK TABLES `aboutus` WRITE;
/*!40000 ALTER TABLE `aboutus` DISABLE KEYS */;
INSERT INTO `aboutus` VALUES (1,'<h2>&Ouml;yk&uuml; &Ouml;nal Kimdir?</h2>\r\n\r\n<p>2005 yılında burslu olarak okumaya hak kazandığım Yeditepe &Uuml;niversitesi İngilizce Tıp Fak&uuml;ltesi&#39;nden mezun olduktan sonra 2006 yılında Ege &Uuml;niversitesi Tıp Fak&uuml;ltesi Psikiyatri Anabilim Dalı&#39;nda başladığım uzmanlık eğitimimi 2011 yılında &#39;alkol-madde kullanım bozukluğu olan denetimli serbestlik olgularının klinik &ouml;zellikleri ve tedavi s&uuml;re&ccedil;lerine&#39; ilişkin tezimi başarıyla tamamlayarak bitirdim. Uzmanlık eğitimim sırasında yurt i&ccedil;i kongre ve eğitimler yanında 2007 yılında Avrupa Psikiyatri Asistanları Birliği yıllık toplantısında T&uuml;rkiye delegasyonunda yer aldım. Aynı yıl İngiltere King&#39;s College&#39;da d&uuml;zenlenen &#39;erken evre psikotik bozukluklar&#39; konulu toplantıya katıldım. Devlet hizmet y&uuml;k&uuml;ml&uuml;l&uuml;ğ&uuml;m&uuml; sırasıyla Torbalı, Tatvan ve Mardin illerindeki g&ouml;revlerimle tamamladıktan sonra 2015 yılında İstanbul Medipol &Uuml;niversitesi Hastaneleri b&uuml;nyesinde psikiyatri uzmanı olarak &ccedil;alışmaya başladım. 2016 yılında T&uuml;rkiye&#39;nin ilk psikiyatri hastanesi olan Fransız Lape Hastanesi&#39;nde yataklı ve ayaktan tedavi birimlerinde psikiyatri uzmanı olarak g&ouml;rev yaptım. 2017 yılından bu yana İzmir &#39;de medikal tedavi ve bireysel psikoterapi yanısıra &nbsp;&ccedil;ift ve aile terapisti olarak da danışanlarımı&nbsp;g&ouml;rmekteyim.</p>\r\n','','');
/*!40000 ALTER TABLE `aboutus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `picture` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,'info@beyretdijital.com','81DC9BDB52D04DC20036DBD8313ED055',NULL,NULL,NULL);
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_title` varchar(255) DEFAULT NULL,
  `post_slug` varchar(255) DEFAULT NULL,
  `post_content` longtext,
  `post_description` longtext,
  `post_image` varchar(255) DEFAULT NULL,
  `post_image_alt` varchar(255) DEFAULT NULL,
  `post_date` datetime DEFAULT NULL,
  `post_status` int(11) DEFAULT NULL,
  `post_views` int(11) DEFAULT NULL,
  `post_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blogs`
--

LOCK TABLES `blogs` WRITE;
/*!40000 ALTER TABLE `blogs` DISABLE KEYS */;
INSERT INTO `blogs` VALUES (1,'Psikoterapi','psikoterapi','','','2019-01-03_23:55:21_psikoterapi.jpg','','2019-01-03 23:55:21',1,0,'2019-01-03 23:55:21');
/*!40000 ALTER TABLE `blogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone2` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `gsm` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `adress` longtext COLLATE utf8_turkish_ci,
  `maplink` longtext COLLATE utf8_turkish_ci,
  `email` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (1,'','0(232) 715 4000','','Ataşehir Mahallesi 8019/16. Sokak No:18 35630 İzmir','https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3123.179701390854!2d27.05637331558418!3d38.483495178839235!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14bbda0c8aa7b645%3A0xecb0067128bfeab8!2zQXRhxZ9laGlyIE1haGFsbGVzaSwgODAxOS8xNi4gU2suIE5vOjE4LCAzNTYzMCDDh2nEn2xpL8Swem1pcg!5e0!3m2!1str!2str!4v1533305446880','info@oykuonal.com');
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery`
--

DROP TABLE IF EXISTS `gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery`
--

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `general`
--

DROP TABLE IF EXISTS `general`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `general` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyName` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `companyLogo` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `general`
--

LOCK TABLES `general` WRITE;
/*!40000 ALTER TABLE `general` DISABLE KEYS */;
INSERT INTO `general` VALUES (1,'Öykü Önal','2019-01-03_21:20:57_2019-01-03_16:59:53_brain-and-head.png');
/*!40000 ALTER TABLE `general` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `keywords`
--

DROP TABLE IF EXISTS `keywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keywords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keywords`
--

LOCK TABLES `keywords` WRITE;
/*!40000 ALTER TABLE `keywords` DISABLE KEYS */;
INSERT INTO `keywords` VALUES (1,'depresyon'),(2,'mutsuz');
/*!40000 ALTER TABLE `keywords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `href` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `access` int(11) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES (1,'','Genel','bookmark',1,0,'parent'),(2,'aboutus.php','Hakkımızda',NULL,1,1,NULL),(3,'contact.php','İletişim Bilgileri',NULL,1,1,''),(4,'company.php','Firma Bilgileri','',1,1,NULL),(5,'social.php','Sosyal Medya','',1,1,NULL),(6,NULL,'Hizmetler','quote',1,0,'parent'),(7,'addservice.php','Hizmet Ekle',NULL,1,6,''),(8,'listservice.php','Hizmetleri Görüntüle ',NULL,1,6,NULL),(9,NULL,'Ürünler','store',0,0,'parent'),(10,'addproduct.php','Ürün Ekle','',0,9,NULL),(11,'listproduct.php','Ürünleri Görüntüle',NULL,0,9,NULL),(12,'gallery.php','Galeri','puzzle-piece',0,0,'main'),(13,'users.php','Kullanıcılar','account',0,0,'main'),(14,NULL,'Blog','book-image',1,0,'parent'),(15,'addblog.php','Blog Ekle',NULL,1,14,''),(16,'listblog.php','Blogları Görüntüle','',1,14,NULL);
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_keywords`
--

DROP TABLE IF EXISTS `post_keywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_keywords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_post_keywords_1_idx` (`keyword_id`),
  KEY `fk_post_keywords_2_idx` (`post_id`),
  CONSTRAINT `fk_post_keywords_1` FOREIGN KEY (`keyword_id`) REFERENCES `keywords` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_post_keywords_2` FOREIGN KEY (`post_id`) REFERENCES `blogs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_keywords`
--

LOCK TABLES `post_keywords` WRITE;
/*!40000 ALTER TABLE `post_keywords` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_keywords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `info` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_alt` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_keywords`
--

DROP TABLE IF EXISTS `service_keywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_keywords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_service_keywords_1_idx` (`service_id`),
  KEY `fk_service_keywords_2_idx` (`keyword_id`),
  CONSTRAINT `fk_service_keywords_1` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_service_keywords_2` FOREIGN KEY (`keyword_id`) REFERENCES `keywords` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_keywords`
--

LOCK TABLES `service_keywords` WRITE;
/*!40000 ALTER TABLE `service_keywords` DISABLE KEYS */;
INSERT INTO `service_keywords` VALUES (3,1,1),(4,2,1);
/*!40000 ALTER TABLE `service_keywords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `content` longtext,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_alt` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'depresyon','Depresyon','user-md','<p>Depresyon, bir ruh halidir. Ancak &ccedil;oğu zaman psikiyatrik bir bozukluğu tanımlamak amacıyla da kullanıldığı i&ccedil;in giderek bir hastalık adı haline gelmiştir. Bir kişiye &quot;depresyonda&quot; olduğu s&ouml;ylendiğinde, bir t&uuml;r ruhsal hastalık durumunda olduğunu anlamaktayız. Zaman zaman herkes kendini kederli, &uuml;zg&uuml;n, mutsuz hatta karamsar hissedebilir. Depresyon hastalığının olağan g&uuml;nl&uuml;k ruh hali değişikliklerinden farkı kişinin sadece duygusal olarak &uuml;zg&uuml;n, mutsuz, moralsiz hissetmesi değil ama bunun yanında; &uuml;mitsiz, &ccedil;aresiz ve karamsar d&uuml;ş&uuml;ncelere sahip olması, kendini bu halde yetersiz ve değersiz olarak kabul etmesi ve hatta intihar d&uuml;ş&uuml;ncesinde olması, davranışsal y&ouml;nden kendini toplumdan uzaklaştırması, i&ccedil;ine kapalılık g&ouml;stermesi, git gide durgunlaşması, hi&ccedil;bir şeyden keyif alamaması ve isteksizlik g&ouml;stermesi ve bedensel y&ouml;nden de uyku d&uuml;zeninin ve beslenmesinin bozulmasıdır. G&uuml;nl&uuml;k olan olayların da insanların ruh hali &uuml;zerinde olumsuz etkileri vardır, ancak depresyondan farklı olarak kişinin bunu &ccedil;&ouml;z&uuml;m&uuml; yokmuş ve kendisi de yetersizmis gibi hissetmemesidir. G&uuml;ndelik olaylar y&uuml;z&uuml;nden morali bozulan kişi olumlu gelişmeler yaşadık&ccedil;a kendini iyi hissederken, depresyondaki kişi olaylarla kendini daha iyi hissetmez. Bu y&uuml;zden b&uuml;t&uuml;n anlık ruh hali değişikliklerini depresyon olarak kabul etmek hatalı bir yaklaşım olmaktadır. Depresyonun yaygınlığı, 2010 yılında yayınlanan bir &ccedil;alışmayı baz alırsak toplumda %8-10 aralığında ortaya &ccedil;ıkmaktadır. Hayat boyu depresyon riski ise erkeklerin onda biri, kadınların ise d&ouml;rt veya beşte biri hayatları boyunca en az bir kere depresyona yakalanmaktadır.</p>\r\n','Depresyon','','2019-01-03_23:41:52_depresyon.jpg','depresyon',1),(2,'panik','Panik','heartbeat','<p>Esas olarak bir anda ortaya &ccedil;ıkan ve zamanla tekrarlayan yoğun sıkıntı ya da korku n&ouml;betleridir. &Ccedil;oğu hastanın &quot;kriz&quot; adını verdiği bu n&ouml;betler PANİK ATAK&#39;tır. Panik Atak, bir anda başlar, git gide şiddetlenir ve 10 dakika i&ccedil;inde de şiddeti en &uuml;st d&uuml;zeye &ccedil;ıkar; &ccedil;oğunlukla 10-30 dakika devam eder ve sonra da kendiliğinden ge&ccedil;er. Panik Bozukluğu, tekrar eden beklenmedik panik Ataklar, ataklar arasında başka Panik Ataklarının olacağına dahi s&uuml;rekli kaygı duyma, panik Atakların &quot;kalp krizi ge&ccedil;irmeye ve &ouml;l&uuml;me sebep olma&quot;, &quot;kontrol&uuml; kaybedip &ccedil;ıldırma&quot; ya da &quot;felce sebep olma&quot; gibi sonu&ccedil;lara yol a&ccedil;abileceğine inanıp s&uuml;rekli &uuml;z&uuml;lme veya ataklara ve k&ouml;t&uuml; sonu&ccedil;lara karşı &ouml;nlem olarak bazı farklı davranışların g&ouml;r&uuml;ld&uuml;ğ&uuml; ruhsal bir rahatsızlıktır. Panik bozukluğu, hi&ccedil;bir sebep yokken bir anda ortaya &ccedil;ıkan g&ouml;ğ&uuml;s ağrısı, g&ouml;ğ&uuml;ste sıkışma, &ccedil;arpıntı, nefes alamama, terleme, titreme, &uuml;ş&uuml;me ya da &uuml;rperme, bazen de bulantı ya da karın ağrısı, baş d&ouml;nmesi, dengesizlik; d&uuml;şecek ya da bayılacakmış gibi olma, uyuşma ya da karıncalanma gibi belirtiler, ile kişiyi dehşet i&ccedil;inde bırakarak oluşur. O an kişi &quot;kalp krizi&quot; ge&ccedil;irmekte olduğunu veya fel&ccedil; ge&ccedil;irdiğini sanarak yoğun bir &quot;&ouml;l&uuml;m korkusu&quot; veya &quot;fel&ccedil; korkusu&quot; yaşar.</p>\r\n','Panik','','2019-01-03_23:43:11_panik.jpg','panik',1),(3,'bipolar','Bipolar','users','<p>Bipolar bozukluk veya manik depresif hastalıklarda, kişilerin motivasyonlarında, d&uuml;ş&uuml;ncelerinde ve ruh halinde belirgin gelgitler g&ouml;zlemlenir. Buna g&ouml;re hastalar ayni anda hem depresif d&ouml;nemler hem de coşkulu veya olağan&uuml;st&uuml; sinirli bir ruh haline sahip d&ouml;nemler ge&ccedil;irirler. İkinci &ccedil;eşit d&ouml;nemler, belirgin bir motivasyon artışıyla birlikte ortaya &ccedil;ıkar. Bu d&ouml;nemler hafif bir bi&ccedil;imde ortaya &ccedil;ıkarsa hipomanik epizotlardan, g&uuml;&ccedil;l&uuml; şekilde ortaya &ccedil;ıkarsa da manik epizotlardan bahsedilir. Ağır manilerde, var olan belirtilere bir psikozun belirtileri eklenir, mesela b&uuml;y&uuml;kl&uuml;k hastalığı veya takip edilme korkusu gibi. Her 100 kişiden 1 illa 3 kişide yaşamı boyunca bir bipolar hastalık g&ouml;zlemlenir. Sadece depresif hastalıkların aksine, bu hastalığın kadın ve erkeklerde g&ouml;r&uuml;nme oranı aynıdır . &Ccedil;oğu zaman bipolar bozukluğu olanlarda ,kişilik bozuklukları yada dikkat eksikliği hiperaktivite bozukluğu gibi başka hastalıklar da g&ouml;r&uuml;l&uuml;r. Herkes bipolar bozukluklara yakalanabilir ve bu hastalıklar &ccedil;oğu zaman yetişkinlik d&ouml;neminin ilk zamanlarında, 18 yaş gibi g&ouml;r&uuml;l&uuml;r. Ancak, yaşamın daha ileri ki safhalarında da, yaşanan kriz veya k&ouml;kl&uuml; değişiklik durumları b&ouml;yle bir ruhsal hastalığa sebep olabilir.</p>\r\n','Bipolar','','2019-01-03_23:43:23_bipo.jpg','bipolar',1),(4,'anksiyete','Anksiyete','stethoscope','<p>Anksiyete bozukluğu olan hastalar panik seviyesinin y&uuml;kseldiği kriz durumlarında k&ouml;t&uuml; bir şey olacakmış gibi duygulara kapılarak i&ccedil;inde bulundukları durumu olduğundan daha k&ouml;t&uuml; g&ouml;r&uuml;rler. Anksiyete yaşamakta olan kişilerde bu his o kadar şiddetlidir ki sanki hi&ccedil; sona ermeyecekmiş gibi gelir ancak anksiyete &ccedil;oğu zaman bilişsel davranış&ccedil;ı terapileriyle tedavi edilebilmektedir. Anksiyeteye sahip hastalarda kaygı seviyesinin y&uuml;ksek olduğu durumlarda yoğun bir panik duygusunun yanı sıra ellerde aşırı terleme, kalp atışlarında hızlanma, nefes almakta zorluk, şiddetli baş ağrıları, mide krampları, sık idrara &ccedil;ıkma belirtileri de g&ouml;r&uuml;l&uuml;r. Araştırmalar, &ccedil;oğunlukla &ccedil;ocukluk yaşlarında ortaya &ccedil;ıkan anksiyete bozukluklarında hem genetik hem de &ccedil;evresel fakt&ouml;rlerin her ikisinin de rol oynadığını ortaya &ccedil;ıkarıyor. &ouml;zellikle, erken yaşlarda yaşanan travmatik olayların kişilerin korku işleme mekanizmalarında hassasiyete neden olarak stres tetikleyicilerine karşı fazla duyarlı hale gelmelerine yol a&ccedil;ıyor.</p>\r\n','Anksiyete','','2019-01-03_23:43:38_anksiyete.jpg','anksiyete',1),(5,'dehb','Dehb','child','<p>DEHB (Dikkat Eksikliği ve Hiperaktivite Bozukluğu) , n&ouml;rolojik bir hastalıktır ve &ccedil;ocuk yaşlarda ortaya &ccedil;ıkmaya başlar. &Ccedil;ok &ccedil;eşitli yollarla ortaya &ccedil;ıkar ve &ccedil;eşitli belirtileri vardır. Bu hastalık n&ouml;ral bağlantı ağlarını işleyişini bozarak &ccedil;ocukların beyin yapısını değiştirir. Bu ise &ccedil;ocukların odaklanmasını, dikkatini, d&uuml;rt&uuml;lerini engellemesinde zorluk &ccedil;ıkartır vs. S&uuml;rekli olarak hareket etmelerinin nedeni de budur. DEHB s&uuml;rekli olarak bir odaklanma sorunu ve tepkileri engelleme yeteneğinde eksiklik barındırır ve bu da otokontrolde zorlamaya sebep olur. Bu sorun okulda odaklanma, sosyal uyumsuzluk ve evdeki uslu davranma kapasitesini ciddi şekilde etkiler. Ayrıca, DEHB &ouml;z g&uuml;ven d&uuml;ş&uuml;kl&uuml;ğ&uuml;ne ve uyku bozukluklarına da sebep olabilir.</p>\r\n','Dehb','','2019-01-03_23:43:50_dehb.jpg','dehb',1),(6,'bagimlilik','Bağımlılık','link','<p>Bağımlılığın davranışsal, sosyal, biyolojik ve genetik sebepleri olabilir. Ancak hi&ccedil;bir sebep bağımlılığı tek başına a&ccedil;ıklayamaz. Bu s&uuml;re&ccedil;te bir&ccedil;ok etken olmasına rağmen, esas olarak biyolojik bir s&uuml;re&ccedil;tir. Kişinin ruhsal ve genetik &ouml;zellikleri, &ccedil;evresel fakt&ouml;rler, maddeye ulaşabilir durumda olmak, aile, &ccedil;evre ve k&uuml;lt&uuml;rel &ouml;zellikler kişinin bağımlı olmasında en &ouml;nemli etkenlerdir. madde kullanan herkes i&ccedil;in bağımlılık riski mevcuttur. Kullanımı g&uuml;venli olan hi&ccedil; bir madde yoktur. Ve yenilik arayışı i&ccedil;indekiler, kolayca risk alabilen, aile yapısı sorunlu, ihmal edilen ergenler, stresle başa &ccedil;ıkabilmek i&ccedil;in madde kullanan ve genetik yatkınlığa sahip kişiler daha b&uuml;y&uuml;k risk altındadırlar. Birey, var olan merakından deneme i&ccedil;in kullanmak ve devamında basit olarak g&ouml;rd&uuml;ğ&uuml; madde kullanımını denetleyebileceğini, istediğinde bırakabileceğini d&uuml;ş&uuml;n&uuml;r. Ancak zamanla kullanım kontrolden &ccedil;ıkarak birey, tahmin ettiğinden fazla madde kullanmaya başlar.</p>\r\n','Bağımlılık','','2019-01-03_23:44:04_bagimlilik.jpg','bağımlılık',1);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social`
--

DROP TABLE IF EXISTS `social`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facebook` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  `googleplus` varchar(255) COLLATE utf8_turkish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social`
--

LOCK TABLES `social` WRITE;
/*!40000 ALTER TABLE `social` DISABLE KEYS */;
INSERT INTO `social` VALUES (1,'https://www.facebook.com/oykuonalmusalar/','https://www.instagram.com/oykumusalar/','','https://tr.linkedin.com/in/%C3%B6yk%C3%BC-%C3%B6nal-musalar-8a7756a2','');
/*!40000 ALTER TABLE `social` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-04  0:04:00
