<script src="assets/js/jquery-2.2.3.min.js"></script>
<!-- Default-JavaScript-File -->

<!-- scripts required  for particle effect -->
<script src='assets/js/particles.min.js'></script>
<script src="assets/js/particles.js"></script>
<!-- //scripts required for particle effect -->

<!-- chart -->
<script src='assets/js/amcharts.js'></script>
<script src='assets/js/chart.js'></script>
<!-- //chart -->
<!-- //gallery-js -->

<!-- stats -->
<script src="assets/js/jquery.waypoints.min.js"></script>
<script src="assets/js/jquery.countup.js"></script>
<script>
    $('.counter').countUp();
</script>
<!-- //stats -->

<!-- flexSlider (for testimonials) -->
<link rel="stylesheet" href="assets/css/flexslider.css" type="text/css" media="screen" property="" />
<script defer src="assets/js/jquery.flexslider.js"></script>
<script>
    $(window).load(function () {
        $('.flexslider').flexslider({
            animation: "slide",
            start: function (slider) {
                $('body').removeClass('loading');
            }
        });
    });
</script>
<!-- //flexSlider (for testimonials) -->

<!-- animation js -->
<script src='assets/js/aos.js'></script>
<script>
    AOS.init({
        easing: 'ease-out-back',
        duration: 1000
    });

</script>
<!-- //animation js -->

<!-- smooth scrolling -->
<script src="assets/js/SmoothScroll.min.js"></script>
<!-- move-top -->
<script src="assets/js/move-top.js"></script>
<!-- easing -->
<script src="assets/js/easing.js"></script>
<!--  necessary snippets for few javascript files -->
<script src="assets/js/online-resume.js"></script>

<script src="assets/js/bootstrap.js"></script>
<!-- Necessary-JavaScript-File-For-Bootstrap -->

<!-- gallery -->
<script src="assets/js/jquery.chocolat.js"></script>
<link rel="stylesheet" href="assets/css/chocolat.css" type="text/css" media="screen">
<!-- light-box -->
<script>
    $(function () {
        $('.gallery a').Chocolat();
    });
</script>