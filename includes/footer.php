<div class="w3ls-footer-grids py-4">
    <div class="container py-xl-5 py-lg-3">
        <div class="row">
            <div class="col-lg-4 w3l-footer-logo">
                <!-- footer logo -->
                <a class="navbar-brand" href="index.php"> <?php echo getCompanyName();?></a>
                <!-- //footer logo -->
            </div>
            <!-- button -->
            <div class="col-lg-8 w3l-footer text-lg-right text-center mt-3">
                <ul class="list-unstyled footer-nav-wthree">
                    <li class="mr-4">
                        <a href="index.php">Anasayfa</a>
                    </li>
                    <li class="mr-4">
                        <a href="hakkimda">Hakkımda</a>
                    </li>
                    <li class="mr-4">
                        <a href="ruhsal-bozukluklar">Ruhsal Bozukluklar</a>
                    </li>
                    <li class="mr-4">
                        <a href="blog">Blog</a>
                    </li>
                    <li class="mr-4">
                        <a href="iletisim">İletişim</a>
                    </li>
                </ul>
            </div>
            <!-- //button -->

        </div>
        <div class="row border-top mt-4 pt-lg-4 pt-3 text-lg-left text-center">
            <!-- copyright -->
            <p class="col-lg-8 copy-right-grids mt-lg-1">© 2018 <?php echo getCompanyName();?>. Tüm Hakları Saklıdır. |
                <a href="<?php echo getBeyretLink(); ?>" target="_blank"><?php echo getBeyret();?></a>
            </p>
            <!-- //copyright -->
            <!-- social icons -->
            <div class="col-lg-4 w3social-icons text-lg-right text-center mt-lg-0 mt-3">
                <ul>
                    <li>
                        <a href="<?php echo getFacebook();?>" class="rounded-circle">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo getLinkedin();?>" class="rounded-circle">
                            <i class="fab fa-linkedin"></i>
                        </a>
                    </li>
                    <li class="pl-2">
                        <a href="<?php echo getInstagram();?>" class="rounded-circle">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- //social icons -->
        </div>
    </div>
</div>
