<header data-aos="fade-down">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="logo text-left">
                <h1>
                    <a class="navbar-brand" href="index.php"> <?php echo getCompanyName();?></a>
                </h1>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon">

                </span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-lg-auto text-lg-right text-center">
                    <li class="nav-item <?php if ($thisPage=="main") echo "active"; ?>">
                        <a class="nav-link" href="index.php">Anasayfa
                        </a>
                    </li>
                    <li class="nav-item <?php if ($thisPage=="about") echo "active"; ?>">
                        <a class="nav-link" href="hakkimda">Hakkımda</a>
                    </li>
                    <li class="nav-item <?php if ($thisPage=="services") echo "active"; ?>">
                        <a class="nav-link" href="ruhsal-bozukluklar">Ruhsal Bozukluklar</a>
                    </li>
                    <li class="nav-item <?php if ($thisPage=="blogs") echo "active"; ?>">
                        <a class="nav-link" href="blog">Blog</a>
                    </li>
                    <li class="nav-item <?php if ($thisPage=="contact") echo "active"; ?>">
                        <a class="nav-link" href="iletisim">İletişim</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
