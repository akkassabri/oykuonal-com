<div class="gallery-grids" >
    <section>
        <ul class="da-thumbs">
          <?php
                $list = getBlogs(3);
                while ($row = $list->fetch_assoc()) {?>
                        <li data-aos="zoom-in">
                            <a href="index.php" target="_blank">
                                <img src="beyretwebadmin/assets/images/blogs/<?php echo $row["post_image"];?>" style="height:300px;"  />
                                <div>
                                  <h5><?php echo strip_tags($row["post_title"]);?></h5>
                                  <p>	<?php echo substr(strip_tags($row["post_content"]),0,200);?>...</p>
                                </div>
                            </a>
                        </li>
          <?php } ?>
        </ul>
    </section>
</div>
