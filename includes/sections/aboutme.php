<div class="row py-xl-3 py-lg-3">
    <div class="col-lg-6 feature pl-lg-3 mt-lg-0 mt-5" data-aos="fade-right">
        <h3 class="title-wthree text-dark mb-4">
            <span class="mb-2">Hakkımda</span></h3>
        <?php echo getAbout();?>
    </div>
    <div class="col-lg-6 feature fea-slider" data-aos="fade-left">
      <center>
        <img src="assets/images/about.jpg" class="img-fluid" alt="" style="height:360px!important;">
      </center>
        <div class="col-lg-13" data-aos="fade-up"
             data-aos-duration="3000">
            <div class="grid-flex bg-white p-5">
                <div class="pl-4 title-edu py-2 mb-5">
                    <i class="fas fa-book-reader mb-2"></i>
                    <h4 class="aboutbtm-head">Eğitim Bilgileri</h4>
                </div>
                <div class="grids-agiles-one">
                    <h5 class="text-dark mb-2">İngilizce Tıp, Tıp Fakültesi (Burslu, 2005)</h5>
                    <p>Yeditepe Üniversitesi (İstanbul)</p>
                </div>
                <div class="grids-agiles-one my-3">
                    <h5 class="text-dark mb-2">Psikiyatri Anablim Dalı, Tıp Fakültesi (2011)</h5>
                    <p>Ege Üniversitesi (İzmir)</p>
                </div>

                <div class="style-book-wthree">
                    <i class="fas fa-book-open"></i>
                </div>
            </div>
        </div>
    </div>
</div>
