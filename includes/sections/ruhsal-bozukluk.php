
    <div class="row agileits-w3layouts-grid pt-md-4" >
        <?php
          $list = getServices();
          while ($row = $list->fetch_assoc()) {?>
            <div class="col-lg-4 services-agile-1" data-aos="zoom-in-up">
                <a href="ruhsal-bozukluklar/<?php echo $row['slug']; ?>">
                <div class="row wthree_agile_us py-md-4 py-3">
                    <div class="col-lg-3 col-sm-2 col-3 pr-lg-2">
                        <div class="wthree_features_grid text-center py-3">
                            <i class="fas fa-<?php echo $row["icon"];?>"></i>
                        </div>
                    </div>
                    <div class="col-lg-9 col-sm-10 col-9 agile-why-text">
                        <h4 class="text-white font-weight-bold mb-3"><?php echo $row["title"];?></h4>
                        <p><?php echo substr($row['content'], 0, 250) . "..."; ?></p>
                    </div>
                </div>
                </a>
            </div>
        <?php  }
        ?>
    </div>
