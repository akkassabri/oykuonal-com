<!DOCTYPE html>
<html lang="tr">
<head>
  <title>İzmir Psikiyatri Öykü Önal | İzmir Psikiyatri Fiyatları | İzmir psikiyatri Uzmanı</title>
    <meta name="description" content="Öykü Önal izmir psikiyatri uzmanı en iyilerinden izmir psikiyatri tavsiye ve izmir psikiyatri fiyatları - izmir psikolog">
    <meta name="keywords" content="izmir psikiyatri uzmanı,izmir psikiyatri tavsiye, izmir psikiyatri fiyatları,izmir psikolog">
	<?php include 'includes/head.php';?>
</head>
<body>
    <div class="mian-content-333">
        <?php include 'includes/header.php';?>
    </div>
    <section class="bottom-banner-w3layouts py-5" id="about">
        <div class="container py-xl-5 py-lg-3">
            <div class="row py-xl-3 py-lg-3">
                <div class="col-lg-12 feature pl-lg-3 mt-lg-0 mt-5" data-aos="fade-left">
                    <h3 class="title-wthree text-dark mb-4">
                        <span class="mb-2">Blog Yazılarım</span>İzmir Psikiyatri</h3>
                    <p>
                      <strong>Öykü önal :İzmir Psikiyatri</strong>
                      Psikiyatri, bir tıp disiplinidir. Kişilerin duygu ve
                      düşüncelerini analiz edip davranışlarındaki bozuklukları türlü
                      metodlarla iyileştirip onlara yardım eder. Psikiyatristler
                      doktordur ve ruhsal hastalıkların tanısını koyup onları tedavi
                      eder ve bu bozuklukların engellenmesi üzerinde çalışırlar. Bu
                      bozukluklar zihinsel olabileceği gibi algısal ya da davranışsal
                      belki de sosyal olabilir. <strong>Psikiyatrist Öykü Önal</strong> bu
                      bozuklukların tedavisi için İzmir’de hastalara ruhsal tedavi
                      uygulamaktadır. Öykü Önal bir <strong>psikiyatri uzmanı</strong>dır.
                      Psikiyatride bir çok farklı tedavi tekniği vardır. Hasta
                      tedavisinde psikiyatrinin alt uzmanlık alanları ve teorik
                      disiplinler arası yardımlaşma önemlidir. Psikiyatride genel
                      olarak psikoterapi en çok kullanılan tedavi şeklidir. <strong>İzmir
                          psikiyatri uzmanı</strong> Öykü Önal da psikoterapi alanında
                      uzmanlaşmıştır.
                    </p>
                </div>
            </div>
        </div>
    </section>
	<footer>
		<?php include 'includes/footer.php';?>
	</footer>
	<?php include 'includes/foot.php';?>
</body>
</html>
