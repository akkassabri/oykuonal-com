<!DOCTYPE html>
<html lang="tr">
<head>
<title>İzmir Psikiyatri Uzmanı Öykü Önal | İzmir Psikiyatri Fiyatları | İzmir psikiyatri Uzmanı</title>
  <meta name="description" content="Öykü Önal izmir psikiyatri uzmanı en iyilerinden izmir psikolog fiyatları izmir psikiyatri klinikleri">
  <meta name="keywords" content="izmir psikiyatri uzmanı,izmir psikiyatri tavsiye,izmir psikolog, izmir psikiyatri klinikleri">
	<?php include 'includes/head.php';?>
</head>
<body>
    <div class="mian-content-333">
        <?php include 'includes/header.php';?>
    </div>
    <section class="bottom-banner-w3layouts py-5" id="about">
        <div class="container py-xl-5 py-lg-3">
            <div class="row py-xl-3 py-lg-3">
                <div class="col-lg-12 feature pl-lg-3 mt-lg-0 mt-5" data-aos="fade-left">
                    <h3 class="title-wthree text-dark mb-4">
                        <span class="mb-2">Blog Yazılarım</span>İzmir Psikiyatri Uzmanı</h3>
                    <p>
                      Psikiyatrik rahatsızlıklar günümüzde çokça yaygınlaşmış ve kronik
                      hale gelmiştir. Bu rahatsızlıkların sebepleri biyolojik, sosyal ve
                      toplumsal veya genetik olabilmektedir. Psikiyatrik rahatsızlıkların
                      ortaya çıkışı hem kişiden hem de dış faktörlerden kaynaklanabilir.
                      Etkenler hastalıktan hastalığa değişiklik gösterebilir. Bir kişide
                      meydana gelen fiziksel bir rahatsızlık psikiyatrik bir rahatsızlığa
                      neden olabilirken aynı şekilde dış ortamda meydana gelen negatif
                      şartlar ve çevre de psikiyatrik rahatsızlıklara sebep
                      olabilmektedir. Ama yine de unutulmaması gereken önemli bir ayrıntı
                      da psikiyatrik rahatsızlığın çoğuna sebep olan başlıca neden ruhsal
                      bozukluklardır.
                      İzmir psikiyatri uzmanı Öykü Önal bu hastalıklar için tamamen
                      bilimsel tedavi yöntemleri ve uygulamaları kullanmaktadır. Ve
                      bunlara paralel gelişen tedavi yöntemlerini takip etmekte ve daha
                      etkin tedaviler uygulamaktadır.
                    </p>
                </div>
            </div>
        </div>
    </section>
	<footer>
		<?php include 'includes/footer.php';?>
	</footer>
	<?php include 'includes/foot.php';?>
</body>
</html>
