<?php $thisPage="services"; ?>
<!DOCTYPE html>
<html lang="tr">
<head>
	<title>Psikiyatrist Öykü Önal | Ruhsal Bozukluklar</title>
	<meta name="keywords" content="" />
	<?php include 'includes/head.php';?>
</head>
<body>
    <div class="mian-content-333">
        <?php include 'includes/header.php';?>
    </div>
	<div class="why-choose-agile py-5" id="services">
		<div class="container py-xl-5 py-lg-3">
				<div class="mb-lg-5 mb-4 text-center">
						<h3 class="title-wthree text-white mb-2" >
								<span class="text-dark mb-2">Psikiyatri</span>Ruhsal Bozukluklar</h3>
								<span class="text-dark mb-2">Ruhsal Bozuklar; kişinin günlük hayatındaki yaşantısını zorlaştıran, çekilmez hale getiren,kişinin anlık yaşantı algısını bozan,geleceğini kararmış gibi hissetmesini,algılamasını düşünmesine yol açan bir dizi nörolojik  veya biyo-nörolojik  etkenler bütününe verilen isimdir.
Depresyon,unipolar ve bipolar bozukluklar,Yaygın Anksiyete Bozukluğu(kaygı-durum bozukluğu),dehb(Dikkat Eksikliği ve Hiperaktive Bozuklukları),Panik Atak,OKB(Obsesif-Kompulsif Bozukluk),Burnout yani Tükenmişlik Sendromu, Özgül Fobiler ,Obesite(Şişmanlık), Yeme Bozuklukları, Sosyal Fobi,Uyuşturucu Madde Bağımlılığı,Nikotin (sigara) Bağımlılığı,Erişkin Tip DEHB/Deb(ADHD) ve Travma Sonrası Stres Bozukluğu başlıca Türkiye Psikiyatri Derneği’nin kabul ettiği ruhsal bozukluklara verilen isimlerdir.
Huzurlu bir ortamda özgüvenli bir şekilde size rahatsızlık veren durumlara çözüm için başvurabileceğiniz kliniğimizde, Psikiyatrist Dr.Öykü Önal’ın danışmanlık yaptığı başlıca uzmanlık alanları ve tanımları kendi ağzından şu şekildedir; </span>
				</div>
				<?php include 'includes/sections/ruhsal-bozukluk.php';?>
		</div>
	</div>
	<footer>
		<?php include 'includes/footer.php';?>
	</footer>
	<?php include 'includes/foot.php';?>
</body>
</html>
