<?php $thisPage="about"; ?>
<!DOCTYPE html>
<html lang="tr">
<head>
	<title>Psikiyatrist Öykü Önal | Hakkımda</title>
	<meta name="keywords" content="" />
	<?php include 'includes/head.php';?>
</head>
<body>
    <div class="mian-content-333">
        <?php include 'includes/header.php';?>
    </div>
		<section class="bottom-banner-w3layouts py-5" id="about">
    <div class="container py-xl-5 py-lg-3">
				<?php include 'includes/sections/aboutme.php';?>
				<div class="row py-xl-3 py-lg-3">
						<div class="col-lg-6 feature fea-slider" data-aos="fade-left">
								<div class="col-lg-13" data-aos="fade-up"
										 data-aos-duration="3000">
										<div class="grid-flex bg-white p-5">
												<div class="pl-4 title-edu py-2 mb-5">
														<i class="fas fa-building mb-2"></i>
														<h4 class="aboutbtm-head">Kuruluşlara Üyelik</h4>
												</div>
												<div class="grids-agiles-one">
														<h5 class="text-dark mb-2">Türkiye Psikiyatri Derneği</h5>
												</div>
												<div class="grids-agiles-one my-3">
														<h5 class="text-dark mb-2">EFPT (Avrupa Psikiyatri Asistanları Federasyonu)</h5>
												</div>
												<div class="grids-agiles-one my-3">
														<h5 class="text-dark mb-2">Türk Tabipler Birliği</h5>
												</div>

												<div class="style-book-wthree">
														<i class="fas fa-book-open"></i>
												</div>
										</div>
								</div>
						</div>
						<div class="col-lg-6 feature fea-slider" data-aos="fade-left">
								<div class="col-lg-13" data-aos="fade-up"
										 data-aos-duration="3000">
										<div class="grid-flex bg-white p-5">
												<div class="pl-4 title-edu py-2 mb-5">
														<i class="fas fa-newspaper mb-2"></i>
														<h4 class="aboutbtm-head">Yayınlar</h4>
												</div>
												<div class="grids-agiles-one">
														<h5 class="text-dark mb-2">"An Unexpected Cause of Hepatotoxicity and Myopathy in A Patient With Coronary Artery Disease: It Is Not Statin”  Koşuyolu Heart Journal  Article in Press</h5>
														<p>Gamze Babur Güler / Ekrem Güler / Gültekin Günhan Demir / Hacı Murat Güneş / Zeynep Işıl Uğurad / Öyku Önal Musalar</p>
												</div>


												<div class="style-book-wthree">
														<i class="fas fa-book-open"></i>
												</div>
										</div>
								</div>
						</div>
				</div>
        <div class="row py-xl-3 py-lg-3">
            <div class="col-lg-12 feature fea-slider" data-aos="fade-left">
                <div class="col-lg-13" data-aos="fade-up"
                     data-aos-duration="3000">
                    <div class="grid-flex bg-white p-5" >
                        <div class="pl-4 title-edu py-2 mb-5">
                            <i class="fas fa-atom mb-2"></i>
                            <h4 class="aboutbtm-head">Bilimsel Aktivite Ve Ödüller</h4>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">8. İzmir Psikanaliz ve Psikoterapi Günleri</h5>
                            <p>İzmir, 2006</p>
                        </div>
                        <div class="grids-agiles-one my-3">
                            <h5 class="text-dark mb-2">“The Early Phase of Psychosis - research & treatment” An International Conference Institute of Psychiatry King’s College</h5>
                            <p>London, 2007</p>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">1. Psiko-onkoloji Çalıştayı</h5>
                            <p>Nevşehir, 2007</p>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">European Federation for All Psychiatric Trainees Annual Forum Turkish Delegation</h5>
                            <p>Athens-Greece, 2007</p>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">Psikotik Spektrum Toplantısı</h5>
                            <p>İstanbul, 2007</p>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">45. Ulusal Psikiyatri Kongresi ve Cinsel İşlev Bozuklukları Sempozyumu</h5>
                            <p>Ankara, 2009</p>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">5. Biennial Conference of the International Society for Bipolar Disorder</h5>
                            <p>İstanbul, 2012</p>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">Psikofarmakoloji Derneği Tedavi Güncellemesi Toplantısı</h5>
                            <p>Antalya, 2012</p>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">DSM-IV için Yapılandırılmış Klinik Görüşme Formu (SCID-I) Uygulamalı Eğitimi Programı Eğitici: Prof. Dr. Soli Sorias</h5>
                            <p>İzmir, Ekim 2006-Ocak 2007</p>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">“Bilişsel Davranışçı Psikoterapiler: Düşünsel Duygulanımcı Davranış Terapisi Kursu” 43. Ulusal Psikiyatri Kongresi</h5>
                            <p>İstanbul, 2007</p>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">“Psikiyatrik Aciller Kursu” 2. Ulusal Psikofarmakoloji Kongresi</h5>
                            <p>İstanbul, 2007</p>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">Bireysel Psikanalitik Yönelimli Psikoterapi Süreci</h5>
                            <p>2008-2009</p>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">“İyi Yayın Uygulamaları Kursu” Ege Üniversitesi Tıp Fakültesi Düzenleyiciler: Doç. Dr. Şebnem Apaydın Yard. Doç. Dr. Hatice Şahin Prof.Dr. Işık Tuğlular</h5>
                            <p>İzmir, 2009</p>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">Introduction to Cognitive Therapy Cognitive Therapy of Depression Cognitive Therapy of Anxiety Disorders Cognitive Therapy of Personality Disorders Director of Education: Leslie Sokol</h5>
                            <p>April 23-24 2009</p>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">Analitik Yönelimli Psikoterapide Etkin İletişim: Duygulanım ve kişilerarası ilişkilere odaklanmak Eğitici: Norbert Hartkamp 11. İzmir Psikanaliz ve Psikoterapi Günleri</h5>
                            <p>İzmir, 2009</p>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">Lundbeck Depresyon Enstitüsü</h5>
                            <p>Antalya, 2009</p>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">Problemli Madde Kullanımı Kullanımının Tahmini: “Capture-recapture” Workshop Eğiticiler: Gordon Hay Maria Gannon Centre fo Drug Misuse Research University of Glasgow UK Ege Üniversitesi Madde Bağımlılığı Toksikoloji ve İlaç Bilimleri Enstitüsü</h5>
                            <p>İzmir, 2009</p>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">Lundbeck Anksiyete Enstitüsü</h5>
                            <p>Antalya, 2010</p>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">Bir Olgunun Psikanalitik Yönelimli Psikoterapi ile 6 ay süreli izlemi Süpervizör: Prof Dr. Işıl Vahip</h5>
                            <p>2010</p>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">“Addiction Course” Ege Üniversitesi Madde Bağımlılığı Toksikoloji ve İlaç Bilimleri Enstitüsü</h5>
                            <p>İzmir, 2011</p>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">Bilişsel ve Davranışçı Terapi Kuramsal Eğitimi (42 saat) Eğitici: Prof. Dr. Hakan Türkçapar</h5>
                            <p>Ankara, 2012</p>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">Bilişsel ve Davranışçı Terapi Uygulamalı Eğitimi Süpervizyon Aşaması Eğitici: Prof. Dr. Hakan Türkçapar</h5>
                            <p>2013 - Devam Etmektedir</p>
                        </div>
                        <div class="grids-agiles-one">
                            <h5 class="text-dark mb-2">Aile ve Çift Terapisi Kuramsal Eğitimi (Avrupa ve Uluslarası Aile Terapileri Birliği’nden sertifiye)</h5>
                            <p>2015 - Devam Etmektedir.</p>
                        </div>

                        <div class="style-book-wthree">
                            <i class="fas fa-book-open"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
	<footer>
		<?php include 'includes/footer.php';?>
	</footer>
	<?php include 'includes/foot.php';?>
</body>
</html>
