<!DOCTYPE html>
<html lang="tr">
<head>
    <?php include 'includes/head.php';?>
    <?php $row=getBlogWithSlug($_GET["slug"])->fetch_assoc();?>
    <meta name="keywords" content="<?php echo getBlogKeywordsWithId($row["id"]);?>"/>
    <meta name="description" content="<?php echo $row["post_description"];?>">
    <title>Psikiyatrist Öykü Önal | <?php echo $row["post_title"];?></title>
</head>
<body>
<div class="mian-content-333">
    <?php include 'includes/header.php';?>
</div>
    <section class="bottom-banner-w3layouts py-5" id="about">
        <div class="container py-xl-5 py-lg-3">
            <div class="row py-xl-3 py-lg-3">
                <div class="col-lg-6 feature fea-slider" data-aos="fade-right">
                    <img src="beyretwebadmin/assets/images/blogs/<?php echo $row["post_image"];?>" class="img-fluid">
                </div>
                <div class="col-lg-6 feature pl-lg-3 mt-lg-0 mt-5" data-aos="fade-left">
                    <h3 class="title-wthree text-dark mb-4">
                        <span class="mb-2">Blog Yazılarım</span><?php echo $row["post_title"];?></h3>
                    <p><?php echo $row["post_content"];?></p>
                </div>
            </div>
        </div>
    </section>
    <footer>
    <?php include 'includes/footer.php';?>
    </footer>
<?php include 'includes/foot.php';?>
</body>
</html>
