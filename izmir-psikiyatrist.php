<!DOCTYPE html>
<html lang="tr">
<head>
  <title>İzmir Psikiyatri Uzmanı Öykü Önal | İzmir Psikiyatri Fiyatları | İzmir psikiyatri Uzmanı</title>
  <meta name="description" content="Öykü Önal izmir psikiyatrist en iyi uzmanı izmir psikiyatri klinikleri ve tedavi yerleri psikiyatri fiyatları">
  <meta name="keywords" content="izmir psikiyatrist,izmir psikiyatri klinikleri,psikolojik tedavi,psikiyatri fiyatları">
	<?php include 'includes/head.php';?>
</head>
<body>
    <div class="mian-content-333">
        <?php include 'includes/header.php';?>
    </div>
    <section class="bottom-banner-w3layouts py-5" id="about">
        <div class="container py-xl-5 py-lg-3">
            <div class="row py-xl-3 py-lg-3">
                <div class="col-lg-12 feature pl-lg-3 mt-lg-0 mt-5" data-aos="fade-left">
                    <h3 class="title-wthree text-dark mb-4">
                        <span class="mb-2">Blog Yazılarım</span>İzmir Psikiyatrist</h3>
                    <p>
                      Bilinç oldukça karmaşık ve şartlara bağlıdır ve tedavisi çok zor
                      olabilir. Düşünce süreçleri, duygular, anılar, hayaller, algılamalar
                      vs.. kalp rahatsızlıkları gibi fiziksel görülerek ele alınamaz.
                      Diğer hastalıklar için fiziksel belirtiler gözlenirken, birçok
                      psikoloji kuramı insan davranışının gözlemlenmesine dayanmaktadır.
                      Uygulamalı bir psikiyatrist hastalarla görüşür, endişelerinin ne
                      olduğunu ve zorluğa neden olan şeyleri öğrenmek için
                      değerlendirmeler yapar. Ve psikolojik danışma ve psikoterapi yoluyla
                      tedavi sağlar. İzmir’de psikiyatrist Öykü Önal hastalarını bilimsel
                      yöntemlerden yararlanarak tedavi etmektedir.

                      Psikiyatristlerin başka rolleri de vardır. Okulda öğrenme zorluğu
                      olan çocukların durumlarını değerlendirmek, zorbalıktan nasıl
                      kaçınılabilineceği ile ilgili seminerler düzenlemek, şirketlerde işe
                      alım ekipleriyle birlikte çalışmak vs…                                                    
                    </p>
                </div>
            </div>
        </div>
    </section>
	<footer>
		<?php include 'includes/footer.php';?>
	</footer>
	<?php include 'includes/foot.php';?>
</body>
</html>
