<?php $thisPage="main"; ?>
<!DOCTYPE html>
<html lang="tr">
<head>
	<title>Pskiyatrist Öykü Önal | İzmir Psikiyatri | İzmir Psikolog</title>
	<?php include 'includes/head.php';?>
</head>
<body>
	<div class="mian-content">
		<?php include 'includes/header.php';?>
		<div id="particles-js"></div>
		<div class="banner-text">
			<div class="effect-text-w3ls">
				<div class="container">
					<h2>Psikiyatrist</h2>
					<span class="mytext1 uppercase mt-2"><?php echo getCompanyName();?></span>
					<span class="mytext2"> Terapizm </span>
					<div class="button-w3ls" data-aos="fade-up">
						 <a href="hakkimda" class="btn btn-sm animated-button thar-three mr-2">Özgeçmiş <i class="fas fa-user"></i></a>
						<a href="iletisim" class="btn btn-sm animated-button thar-three mr-2">Randevu Al <i class="fas fa-thumbtack"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<section class="bottom-banner-w3layouts py-5" id="about">
		<div class="container py-xl-5 py-lg-3">
				<?php include 'includes/sections/aboutme.php';?>
		</div>
	</section>
	<!-- //banner bottom -->

	<!-- services -->
	<div class="why-choose-agile py-5" id="services">
		<div class="container py-xl-5 py-lg-3">
				<div class="mb-lg-5 mb-4 text-center">
						<h3 class="title-wthree text-white mb-2" >
								<span class="text-dark mb-2">Psikiyatri</span>Ruhsal Bozukluklar</h3>
				</div>
				<?php include 'includes/sections/ruhsal-bozukluk.php';?>
		</div>
	</div>
	<!-- //services -->

    <!-- clients -->
    <div class="testimonials py-5" id="clients">
        <div class="container py-xl-5 py-lg-3">
            <!-- heading title -->
            <div class="mb-sm-5 mb-4 text-center">
                <h3 class="title-wthree text-dark mb-2">
                    Hasta Yorumları</h3>

            </div>
            <!-- //heading title -->
            <div class="row">
                <div class="col-lg-12 w3_testimonials_grids">
                    <section class="slider">
                        <div class="flexslider">
                            <ul class="slides">
                                <li>
                                    <div class="w3_testimonials_grid px-xl-5 px-4 mt-xl-4 mx-auto">
                                        <h4 class="text-dark text-center font-weight-light">
                                            <i class="fas fa-quote-left mr-3"></i>Öykü Hanım gerçekten profesyonelce çalışıyor. İlgi ve alakası çok iyi. Hastasının sorununu hemen anlıyor. Bu konuda çok başarılı. 3-4 seans katıldım bana oldukça faydası oldu, gayet memnun kaldım. Bir süre ara vermeye karar verdik, daha sonra tekrardan danışanı olacağım, bu şekilde anlaştık en son. <i class="fas fa-quote-right ml-3"></i>
                                        </h4>
                                        <div class="row mt-5">

                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="w3_testimonials_grid px-xl-5 px-4 mt-xl-4 mx-auto">
                                        <h4 class="text-dark text-center font-weight-light">
                                            <i class="fas fa-quote-left mr-3"></i>Çok cana yakın, işini bilen buna istinaden tedavi hakkında plan yapan bir danışman. İlk görüşme sonrası gayet olumluydu. Yapılması gerekenlerin planı yapıldı ve en kısa süre içerisinde de tedaviye başlayacağız. <i class="fas fa-quote-right ml-3"></i>
                                        </h4>
                                        <div class="row mt-5">

                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="w3_testimonials_grid px-xl-5 px-4 mt-xl-4 mx-auto">
                                        <h4 class="text-dark text-center font-weight-light">
                                            <i class="fas fa-quote-left mr-3"></i>Yumuşak anlatımı ve dinlemesiyle tedavimize yardım etti. Bilimsel tavsiyeleri hayatımıza ışık oldu. Teşekkür ederim Öykü Hanım.<i class="fas fa-quote-right ml-3"></i>
                                        </h4>
                                        <div class="row mt-5">

                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <!-- //clients -->

	<!-- education -->
	<div class="info-w3layouts" id="education">
		<div class="w3l-overlay py-5">
			<div class="container py-xl-5 py-lg-3">
				<div class="edu-agile-info">
					<div class="row">
						<div class="col-lg-12" data-aos="fade-up"
						data-aos-duration="3000">
							<div class="grid-flex bg-white p-5">
								<div class="pl-4 title-edu py-2 mb-3">
                  <h5 class="aboutbtm-head">Uzmanlık Alanım</h5>
									<h2 class="aboutbtm-head">Psikoterapi</h2>
								</div>
								<div class="grids-agiles-one">
									<p>Detaylı bir bilgi alınması için ilk olarak terapist ile hasta arasında güvenin oluşması gereklidir. Bu ilişki bir seansta da oluşabilir ya da doğası gereği beklenenden daha uzun da sürebilir. Psikoterapi yalanı temel alamaz, bu zarara neden olur. Karşılıklı olarak her iki tarafın da bir birine güvenmesi zorunludur. İlk olarak kişilik analizi ve türlü testler yapılır. Bu testlerin sonuçları hastaya aktarılır. Çocuk ise mutlaka aile bilgilendirilir. Ailenin de terapiye katılmasını gerektiren durumları terapist belirler. Terapi seanslarında içgörünün sağlıklı olması temel alındığından, edinilen bilgiler ve çıkarılan sonuçlar hakkında kişi detaylı olarak bilgilendirilir ve yol haritası birlikte çizilir.</p>
								</div>
								<div class="style-book-wthree">
									<i class="fas fa-book-open"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //education -->

	<!-- projects -->
	<div class="gallery py-5" id="projects">
		<div class="container py-xl-5 py-lg-3">
			<!-- heading title -->
			<div class="mb-lg-5 mb-4 text-center">
				<h3 class="title-wthree text-dark mb-2">
					<span class="mb-2">Blog</span>Son Yazılarım</h3>
			</div>
			<?php include 'includes/sections/blogs.php';?>
		</div>
	</div>
	<!--//gallery-->

	<footer>
		<?php include 'includes/footer.php';?>
	</footer>
	<?php include 'includes/foot.php';?>
</body>
</html>
