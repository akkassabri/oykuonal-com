<?php $thisPage="contact"; ?>
<!DOCTYPE html>
<html lang="tr">
<head>
    <title>Psikiyatrist Öykü Önal | İletişim</title>
    <meta name="keywords" content="" />
    <?php include 'includes/head.php';?>
</head>
<body>
<div class="mian-content-333">
    <?php include 'includes/header.php';?>
</div>
<!-- contact -->
	<section class="banner-bottom-w3layouts pt-5">
		<div class="container-fluid pt-xl-5 pt-lg-3">
			<!-- heading title -->

			<!-- //heading title -->
			<!-- contact address -->
			<div class="container">
				<div class="address row py-sm-5 pt-sm-0 pt-3 pb-sm-0 pb-5 mb-sm-5">
					<div class="col-lg-4 address-grid" data-aos="zoom-out-up">
						<div class="row address-info">
							<div class="col-3 address-left text-lg-center text-right">
								<i class="far fa-map text-center"></i>
							</div>
							<div class="col-9 address-right text-left">
								<h6 class="ad-info text-uppercase mb-2">Adres</h6>
								<p> <?php echo getAddress(); ?></p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 address-grid my-lg-0 my-4" data-aos="zoom-out-up">
            <a href="mailto:<?php echo getEmail(); ?>">
  						<div class="row address-info">
  							<div class="col-3 address-left text-lg-center text-right">
  								<i class="far fa-envelope text-center"></i>
  							</div>
  							<div class="col-9 address-right text-left">
  								<h6 class="ad-info text-uppercase mb-2">Eposta</h6>
  								<p>	<?php echo getEmail(); ?>
  								</p>
  							</div>
  						</div>
            </a>
					</div>
					<div class="col-lg-4 address-grid" data-aos="zoom-out-up">
            <a href="tel:<?php echo getPhone(); ?>">
  						<div class="row address-info">
  							<div class="col-3 address-left text-lg-center text-right">
  								<i class="fas fa-mobile-alt text-center"></i>
  							</div>
  							<div class="col-9 address-right text-left">
  								<h6 class="ad-info text-uppercase mb-2">Telefon</h6>
  								<p><?php echo getPhone(); ?></p>
  							</div>
  						</div>
            </a>
					</div>
				</div>
			</div>
			<!-- //contact address -->
				<div class="row" data-aos="fade-down"
				data-aos-easing="linear"
				data-aos-duration="1500">
					<!-- map -->
					<div class="col-lg-6 map">
                          <iframe src="<?php echo getMap(); ?>" width="600" height="450" frameborder="0" style="border:0" allowfullscreen>
                        </iframe>
                         </div>
                         <!-- //map -->
					<!-- contact form -->
					<div class="col-lg-6 main_grid_contact">
						<div class="form">
							<h4 class="mb-4 text-left">Benimle İletişime Geçebilirsiniz</h4>
							<form action="#" method="post">
								<div class="form-group">
									<label class="my-2">İsim</label>
									<input class="form-control" type="text" name="Name" placeholder="" required="">
								</div>
								<div class="form-group">
									<label class="my-2">Eposta</label>
									<input class="form-control" type="email" name="Email" placeholder="" required="">
								</div>
								<div class="form-group">
									<label class="my-2">Mesaj</label>
									<textarea id="textarea" name="message" placeholder=""></textarea>
								</div>
								<div class="input-group1">
									<input class="form-control" type="submit" value="Gönder">
								</div>
							</form>
						</div>
					</div>
					<!-- //contact form -->
				</div>
		</div>
	</section>
	<!-- //contact -->
<footer>
    <?php include 'includes/footer.php';?>
</footer>
<?php include 'includes/foot.php';?>
</body>
</html>
