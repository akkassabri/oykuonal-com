<?php $thisPage="blogs"; ?>
<!DOCTYPE html>
<html lang="tr">
<head>
	<title>Psikiyatrist Öykü Önal | Blog</title>
	<meta name="keywords" content="" />
	<?php include 'includes/head.php';?>
</head>
<body>
    <div class="mian-content-333">
        <?php include 'includes/header.php';?>
    </div>
    <div class="py-5">
        <div class="container py-xl-5 py-lg-3">
            <div class="gallery-grids" >
                <section>
                    <ul class="da-thumbs">
											<?php
									          $list = getBlogs();
									          while ($row = $list->fetch_assoc()) {?>
						                        <li data-aos="zoom-in">
						                            <a href="index.php" target="_blank">
						                                <img src="beyretwebadmin/assets/images/blogs/<?php echo $row["post_image"];?>" style="height:300px;"  />
						                                <div>
																							<h5><?php echo strip_tags($row["post_title"]);?></h5>
						                                  <p>	<?php echo substr(strip_tags($row["post_content"]),0,200);?>...</p>
						                                </div>
						                            </a>
						                        </li>
											<?php } ?>
                    </ul>
                </section>
            </div>
        </div>
    </div>
	<footer>
		<?php include 'includes/footer.php';?>
	</footer>
	<?php include 'includes/foot.php';?>
</body>
</html>
